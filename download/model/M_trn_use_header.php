<?php namespace App\Models\transaction; 
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Aug 09, 2021                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
use CodeIgniter\Model;

class M_trn_use_header extends Model
{
     /* START PRIVATE VARIABLES */
     protected $DBGroup = '';
     protected $table = 'trn_use_header';
     protected $primaryKey = 'use_header_id';
     protected $returnType = 'array';
     protected $db = null;

     protected $useHeaderId;
     protected $rabHeaderId;
     protected $useDate;
     protected $useRemarks;
     protected $isActive;
     protected $inputPic;
     protected $inputDate;
     protected $editPic;
     protected $editDate;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */ 
     public function __construct()
     {
     	parent::__construct();
     	$this->db = \Config\Database::connect($this->DBGroup, false);
          $this->useHeaderId = 0;
          $this->rabHeaderId = 0;
          $this->useDate = '0000-00-00';
          $this->useRemarks = '';
          $this->isActive = 0;
          $this->inputPic = '';
          $this->inputDate = '1700-01-01 00:00:00';
          $this->editPic = '';
          $this->editDate = '1700-01-01 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setUseHeaderId($aUseHeaderId)
     {
     	$this->useHeaderId = $aUseHeaderId;
     }
     public function getUseHeaderId()
     {
     	return $this->useHeaderId;
     }
     public function setRabHeaderId($aRabHeaderId)
     {
     	$this->rabHeaderId = $aRabHeaderId;
     }
     public function getRabHeaderId()
     {
     	return $this->rabHeaderId;
     }
     public function setUseDate($aUseDate)
     {
     	$this->useDate = $aUseDate;
     }
     public function getUseDate()
     {
     	return $this->useDate;
     }
     public function setUseRemarks($aUseRemarks)
     {
     	$this->useRemarks = $aUseRemarks;
     }
     public function getUseRemarks()
     {
     	return $this->useRemarks;
     }
     public function setIsActive($aIsActive)
     {
     	$this->isActive = $aIsActive;
     }
     public function getIsActive()
     {
     	return $this->isActive;
     }
     public function setInputPic($aInputPic)
     {
     	$this->inputPic = $aInputPic;
     }
     public function getInputPic()
     {
     	return $this->inputPic;
     }
     public function setInputDate($aInputDate)
     {
     	$this->inputDate = $aInputDate;
     }
     public function getInputDate()
     {
     	return $this->inputDate;
     }
     public function setEditPic($aEditPic)
     {
     	$this->editPic = $aEditPic;
     }
     public function getEditPic()
     {
     	return $this->editPic;
     }
     public function setEditDate($aEditDate)
     {
     	$this->editDate = $aEditDate;
     }
     public function getEditDate()
     {
     	return $this->editDate;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function ins()
     {
     	if($this->useHeaderId =="" || $this->useHeaderId == NULL) 
     	{
          	$this->useHeaderId = 0; 
     	}
     	if($this->rabHeaderId =="" || $this->rabHeaderId == NULL) 
     	{
          	$this->rabHeaderId = 0; 
     	}
     	if($this->useDate =="" || $this->useDate == NULL) 
     	{
          	$this->useDate = '1700-01-01'; 
     	}
     	if($this->useRemarks =="" || $this->useRemarks == NULL) 
     	{
          	$this->useRemarks = ''; 
     	}
     	if($this->isActive =="" || $this->isActive == NULL) 
     	{
          	$this->isActive = 0; 
     	}
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{
          	$this->inputPic = ''; 
     	}
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	}
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{
          	$this->editPic = ''; 
     	}
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{
          	$this->editDate = '1700-01-01 00:00:00'; 
     	}

     	$stQuery  = "INSERT INTO ".$this->table." ";
     	$stQuery .= "( ";
     	$stQuery .=   "use_header_id,"; 
     	$stQuery .=   "rab_header_id,"; 
     	$stQuery .=   "use_date,"; 
     	$stQuery .=   "use_remarks,"; 
     	$stQuery .=   "is_active,"; 
     	$stQuery .=   "input_pic,"; 
     	$stQuery .=   "input_date,"; 
     	$stQuery .=   "edit_pic,"; 
     	$stQuery .=   "edit_date"; 
     	$stQuery .= ") "; 
     	$stQuery .= "VALUES "; 
     	$stQuery .= "( "; 
     	$stQuery .=   " ".$this->useHeaderId.",";
     	$stQuery .=   " ".$this->rabHeaderId.",";
     	$stQuery .=   "'".$this->db->escapeString($this->useDate)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->useRemarks)."',";
     	$stQuery .=   " ".$this->isActive.",";
     	$stQuery .=   "'".$this->db->escapeString($this->inputPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputDate)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editDate)."'";
     	$stQuery .= "); "; 
     	$this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START QUERY INSERT */
     public function getInsQuery()
     {
     	if($this->useHeaderId =="" || $this->useHeaderId == NULL) 
     	{
          	$this->useHeaderId = 0; 
     	}
     	if($this->rabHeaderId =="" || $this->rabHeaderId == NULL) 
     	{
          	$this->rabHeaderId = 0; 
     	}
     	if($this->useDate =="" || $this->useDate == NULL) 
     	{
          	$this->useDate = '1700-01-01'; 
     	}
     	if($this->useRemarks =="" || $this->useRemarks == NULL) 
     	{
          	$this->useRemarks = ''; 
     	}
     	if($this->isActive =="" || $this->isActive == NULL) 
     	{
          	$this->isActive = 0; 
     	}
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{
          	$this->inputPic = ''; 
     	}
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	}
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{
          	$this->editPic = ''; 
     	}
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{
          	$this->editDate = '1700-01-01 00:00:00'; 
     	}

     	$stQuery  = "INSERT INTO ".$this->table." ";
     	$stQuery .= "( ";
     	$stQuery .=   "use_header_id,"; 
     	$stQuery .=   "rab_header_id,"; 
     	$stQuery .=   "use_date,"; 
     	$stQuery .=   "use_remarks,"; 
     	$stQuery .=   "is_active,"; 
     	$stQuery .=   "input_pic,"; 
     	$stQuery .=   "input_date,"; 
     	$stQuery .=   "edit_pic,"; 
     	$stQuery .=   "edit_date"; 
     	$stQuery .= ") "; 
     	$stQuery .= "VALUES "; 
     	$stQuery .= "( "; 
     	$stQuery .=   " ".$this->useHeaderId.",";
     	$stQuery .=   " ".$this->rabHeaderId.",";
     	$stQuery .=   "'".$this->db->escapeString($this->useDate)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->useRemarks)."',";
     	$stQuery .=   " ".$this->isActive.",";
     	$stQuery .=   "'".$this->db->escapeString($this->inputPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputDate)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editDate)."'";
     	$stQuery .= "); "; 
     	return $stQuery; 
     }
     /* END QUERY INSERT */
     /* START UPDATE */
     public function upd($id)
     {
     	if($this->useHeaderId =="" || $this->useHeaderId == NULL) 
     	{ 
          	$this->useHeaderId = 0; 
     	} 
     	if($this->rabHeaderId =="" || $this->rabHeaderId == NULL) 
     	{ 
          	$this->rabHeaderId = 0; 
     	} 
     	if($this->useDate =="" || $this->useDate == NULL) 
     	{ 
          	$this->useDate = '1700-01-01'; 
     	} 
     	if($this->useRemarks =="" || $this->useRemarks == NULL) 
     	{ 
          	$this->useRemarks = ''; 
     	} 
     	if($this->isActive =="" || $this->isActive == NULL) 
     	{ 
          	$this->isActive = 0; 
     	} 
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{ 
          	$this->inputPic = ''; 
     	} 
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{ 
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	} 
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{ 
          	$this->editPic = ''; 
     	} 
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{ 
          	$this->editDate = '1700-01-01 00:00:00'; 
     	} 

     	$stQuery  = "UPDATE ".$this->table." "; 
     	$stQuery .= "SET "; 
     	$stQuery .=   'use_header_id ='.$this->useHeaderId.','; 
     	$stQuery .=   'rab_header_id ='.$this->rabHeaderId.','; 
     	$stQuery .=   "use_date ='".$this->db->escapeString($this->useDate)."', "; 
     	$stQuery .=   "use_remarks ='".$this->db->escapeString($this->useRemarks)."', "; 
     	$stQuery .=   'is_active ='.$this->isActive.','; 
     	$stQuery .=   "input_pic ='".$this->db->escapeString($this->inputPic)."', "; 
     	$stQuery .=   "input_date ='".$this->db->escapeString($this->inputDate)."', "; 
     	$stQuery .=   "edit_pic ='".$this->db->escapeString($this->editPic)."', "; 
     	$stQuery .=   "edit_date ='".$this->db->escapeString($this->editDate)."' "; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'use_header_id = '.$this->db->escapeString($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START QUERY UPDATE */
     public function getUpdQuery($id)
     {
     	if($this->useHeaderId =="" || $this->useHeaderId == NULL) 
     	{ 
          	$this->useHeaderId = 0; 
     	} 
     	if($this->rabHeaderId =="" || $this->rabHeaderId == NULL) 
     	{ 
          	$this->rabHeaderId = 0; 
     	} 
     	if($this->useDate =="" || $this->useDate == NULL) 
     	{ 
          	$this->useDate = '1700-01-01'; 
     	} 
     	if($this->useRemarks =="" || $this->useRemarks == NULL) 
     	{ 
          	$this->useRemarks = ''; 
     	} 
     	if($this->isActive =="" || $this->isActive == NULL) 
     	{ 
          	$this->isActive = 0; 
     	} 
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{ 
          	$this->inputPic = ''; 
     	} 
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{ 
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	} 
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{ 
          	$this->editPic = ''; 
     	} 
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{ 
          	$this->editDate = '1700-01-01 00:00:00'; 
     	} 

     	$stQuery  = "UPDATE ".$this->table." "; 
     	$stQuery .= "SET "; 
     	$stQuery .=   'use_header_id ='.$this->useHeaderId.','; 
     	$stQuery .=   'rab_header_id ='.$this->rabHeaderId.','; 
     	$stQuery .=   "use_date ='".$this->db->escapeString($this->useDate)."', "; 
     	$stQuery .=   "use_remarks ='".$this->db->escapeString($this->useRemarks)."', "; 
     	$stQuery .=   'is_active ='.$this->isActive.','; 
     	$stQuery .=   "input_pic ='".$this->db->escapeString($this->inputPic)."', "; 
     	$stQuery .=   "input_date ='".$this->db->escapeString($this->inputDate)."', "; 
     	$stQuery .=   "edit_pic ='".$this->db->escapeString($this->editPic)."', "; 
     	$stQuery .=   "edit_date ='".$this->db->escapeString($this->editDate)."' "; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'use_header_id = '.$this->db->escapeString($id).''; 
     	return $stQuery; 
     }
     /* END QUERY UPDATE */
     /* START DELETE */
     public function del($id)
     {
     	$stQuery  = "DELETE FROM ".$this->table." ";      
     	$stQuery .= "WHERE use_header_id = ".$this->db->escapeString($id)."";
     	$db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$stQuery  = "SELECT * FROM ".$this->table." "; 
     	$query  = $this->db->query($stQuery); 
     	$arrayList = $query->getResultArray(); 
     	return $arrayList; 
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$stQuery  = "SELECT * FROM ".$this->table." ";     
     	$stQuery .= "WHERE use_header_id = ".$this->db->escapeString($id)."";
     	$query  = $this->db->query($stQuery); 
     	$arrayRow = $query->getRowArray(); 
     	return $arrayRow; 
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function setObjectById($id)
     {
     	$stQuery  = "SELECT * FROM ".$this->table." "; 
     	$stQuery .= "WHERE use_header_id = ".$this->db->escapeString($id)."";
     	$query  = $this->db->query($stQuery); 
     	$arrayRow = $query->getRowArray(); 
     	$this->useHeaderId = $arrayRow['use_header_id']; 
     	$this->rabHeaderId = $arrayRow['rab_header_id']; 
     	$this->useDate = $arrayRow['use_date']; 
     	$this->useRemarks = $arrayRow['use_remarks']; 
     	$this->isActive = $arrayRow['is_active']; 
     	$this->inputPic = $arrayRow['input_pic']; 
     	$this->inputDate = $arrayRow['input_date']; 
     	$this->editPic = $arrayRow['edit_pic']; 
     	$this->editDate = $arrayRow['edit_date']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->useHeaderId = 0; 
     	$this->rabHeaderId = 0; 
     	$this->useDate = '1700-01-01'; 
     	$this->useRemarks = ''; 
     	$this->isActive = 0; 
     	$this->inputPic = ''; 
     	$this->inputDate = '1700-01-01 00:00:00'; 
     	$this->editPic = ''; 
     	$this->editDate = '1700-01-01 00:00:00'; 
     }
     /* END OF RESET VALUES */
     /* START OF ID GENERATOR */
     public function generateId($columnId)
     {
		$strSql = "SELECT ";
		$strSql .= " CASE WHEN id_year = curr_ym THEN ";
		$strSql .= "   CONCAT(id_year,LPAD(CAST( (CAST(RIGHT(fi,5) AS INTEGER)+1) AS CHAR),5,'0')) ";
		$strSql .= " ELSE ";
		$strSql .= "   CONCAT(curr_ym, '00001') ";
		$strSql .= " END AS doc_id ";
		$strSql .= "FROM ";
		$strSql .= " (SELECT ";
		$strSql .= "   MAX(".$columnId.") fi,";
		$strSql .= "   LEFT(".$columnId.",4) id_year,";
		$strSql .= "   current_date dt,";
		$strSql .= "   RIGHT(".$columnId.", 5) curr_id,";
		$strSql .= "   CONCAT(";
		$strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2), ";
		$strSql .= "   LPAD(";
		$strSql .= "      CAST(DATE_FORMAT(NOW(), '%m') AS CHAR),2,'0'";
		$strSql .= "    )";
		$strSql .= "   ) curr_ym,";
		$strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2) tYear,";
		$strSql .= "   CAST(DATE_FORMAT(NOW(), '%m') AS CHAR) tMonth ";
		$strSql .= " FROM ";
		$strSql .= "   ".$this->table." ";
		$strSql .= " GROUP BY ".$columnId.") a ";
		$strSql .= " ORDER BY doc_id DESC LIMIT 1 ";
		$query  = $this->db->query($strSql); 
		$arrayRow = $query->getRowArray(); 
		if(!isset($arrayRow))
		{
			$curYM = date('ym');
			$arrayRow['doc_id'] = $curYM.'00001';
		}
		return $arrayRow;
     }
     /* END OF ID GENERATOR */
}
