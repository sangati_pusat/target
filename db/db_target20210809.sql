/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.1.45-MariaDB : Database - db_target
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_target` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_target`;

/*Table structure for table `mst_menu` */

DROP TABLE IF EXISTS `mst_menu`;

CREATE TABLE `mst_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(4) DEFAULT NULL,
  `menu_name` varchar(50) NOT NULL,
  `url` varchar(75) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `menu_group` varchar(20) NOT NULL,
  `description` text,
  `no` int(2) DEFAULT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `mst_menu` */

insert  into `mst_menu`(`menu_id`,`app_code`,`menu_name`,`url`,`icon`,`menu_group`,`description`,`no`,`active`) values 
(1,'PURC','Supplier','master/supplier','fa fa-tasks','Master',NULL,1,1),
(2,'PURC','Items','master/items','fa fa-tasks','Master',NULL,2,1),
(3,'PURC','Purchase Requisition','transaction/purchase_requisition','fa fa-laptop','Transaction',NULL,2,1),
(5,'PURC','Users','master/users','fa fa-tasks','Master',NULL,3,1),
(6,'PURC','Purchase Order','transaction/purchase_order','fa fa-laptop','Transaction',NULL,3,1),
(8,'PURC','Project','master/project','fa fa-tasks','Master',NULL,4,0),
(11,'PURC','Warehouse','master/warehouse','fa fa-tasks','Master',NULL,5,1),
(12,'PURC','Outgoing Stock','stok/outgoing_stok','fa fa-window-restore','Stock',NULL,2,1),
(13,'PURC','Incoming Stock','stok/incoming_stok','fa fa-window-restore','Stock',NULL,1,1),
(15,'PURC','Stock','report/stok/current_stok','mdi mdi-move-resize-variant','Report',NULL,3,1),
(19,'PURC','PO Payment','transaction/po_payment','fa fa-laptop','Transaction',NULL,3,1),
(20,'PURC','PO Down Payment','transaction/po_dp','fa fa-laptop','Transaction',NULL,4,1),
(21,'PURC','Crew','master/crew','fa fa-tasks','Master',NULL,6,0),
(22,'PURC','Ikan','master/fish','fa fa-tasks','Master',NULL,7,0),
(24,'PURC','Saldo Awal','stok/saldo_awal','fa fa-window-restore','Stock',NULL,3,1),
(25,'PURC','Customer','master/customer','fa fa-tasks','Master',NULL,8,1),
(26,'PURC','Tingkatan Ikan','master/fish_grade','fa fa-tasks','Master',NULL,9,0),
(27,'PURC','History Stock','report/stok/history_stok','mdi mdi-move-resize-variant','Report',NULL,2,1),
(28,'PURC','Unit','master/unit','fa fa-tasks','Master',NULL,10,1),
(29,'PURC','Pinjaman Project','loan/project_loan','fa fa-tasks','Pinjaman',NULL,1,0),
(30,'PURC','Pinjaman Crew','loan/crew_loan','fa fa-tasks','Pinjaman',NULL,2,0),
(31,'PURC','Konversi Barang','stok/unit_conversion','fa fa-window-restore','Stock','(NULL)',4,1),
(32,'PURC','Recallculate','stok/trn_stok/view_recallculate','fa fa-window-restore','Stock',NULL,5,1),
(33,'PURC','Items Project','report/stok/items_project','mdi mdi-move-resize-variant','Report',NULL,4,0),
(34,'PURC','Coa','master/coa','fa fa-tasks','Master',NULL,11,1),
(35,'PURC','Config Journal','master/config_journal','fa fa-tasks','Master',NULL,12,1),
(36,'OMZ','Sales','omzet/sales','fa fa-window-restore','Omzet','(NULL)',4,1),
(37,'OMZ','Invoice Payment','omzet/sales_payment','fa fa-window-restore','Omzet',NULL,7,1),
(38,'OMZ','Omzet Alocation','omzet/allocation','fa fa-window-restore','Omzet',NULL,9,0),
(39,'FIN','Journal','finance/journal','mdi mdi-chart-bubble','Finance',NULL,1,1),
(40,'PURC','Accounts payable Suppliers','report/supplier/account_payable_suppliers','mdi mdi-move-resize-variant','Report',NULL,6,1),
(41,'FIN','Closing Journal','finance/closing_journal','mdi mdi-chart-bubble','Finance',NULL,2,1),
(42,'FIN','Manual Journal','finance/manual_journal','mdi mdi-chart-bubble','Finance',NULL,3,1),
(44,'FIN','Bank / Cash Journal','finance/assets_journal','mdi mdi-chart-bubble','Finance',NULL,4,1),
(45,'PURC','Laba Rugi','report/finance/laba_rugi','mdi mdi-move-resize-variant','Report',NULL,7,1),
(46,'PURC','Neraca','report/finance/neraca','mdi mdi-move-resize-variant','Report',NULL,8,1),
(47,'PURC','Buku Besar','report/finance/buku_besar','mdi mdi-move-resize-variant','Report',NULL,9,1),
(48,'PURC','Price History','report/omzet/history_price','mdi mdi-move-resize-variant','Report',NULL,10,1),
(49,'PURC','Sisa Hutang Supplier / Project','report/finance/sisa_hutang','mdi mdi-move-resize-variant','Report',NULL,10,1),
(50,'OMZ','Delivery Order','omzet/delivery_order','fa fa-window-restore','Omzet',NULL,5,1),
(51,'OMZ','Invoice','omzet/invoice','fa fa-window-restore','Omzet',NULL,6,1),
(52,'OMZ','DP Sales Order','omzet/so_dp','fa fa-window-restore','Omzet',NULL,8,1);

/*Table structure for table `mst_user` */

DROP TABLE IF EXISTS `mst_user`;

CREATE TABLE `mst_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `signature` varchar(200) DEFAULT NULL,
  `phone_imei` varchar(25) DEFAULT NULL,
  `id_user_group` varchar(30) NOT NULL,
  `id_user_level` int(5) NOT NULL,
  `user_app_level` int(1) DEFAULT NULL COMMENT 'Tingkatan pada approve PR',
  `last_login` datetime DEFAULT NULL,
  `last_ip` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mst_user` */

insert  into `mst_user`(`user_id`,`nip`,`name`,`username`,`password`,`signature`,`phone_imei`,`id_user_group`,`id_user_level`,`user_app_level`,`last_login`,`last_ip`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'123456789','Admin','admin','asd','admin.png',NULL,'1',3,1,'2021-04-15 04:31:47','::1',1,1,'2018-06-11 08:15:13',NULL,NULL),
(2,'123456790','User 1','user1','asd','user1.png',NULL,'4',2,2,'2018-09-26 08:53:53','192.168.10.38',1,1,'2018-06-11 08:17:59',NULL,NULL),
(3,'123456791','User 2','user2','asd','user2.jpg',NULL,'4',2,3,'2018-09-19 14:56:16','192.168.10.14',1,1,'2018-06-11 08:19:47',1,'2018-06-11 11:13:55'),
(4,'123456792','User 3','user3','asd',NULL,NULL,'4',1,NULL,NULL,NULL,1,1,'2018-06-11 08:20:50',1,'2018-08-31 13:46:54'),
(5,'3095','Usup Purliawan','usup','1234',NULL,NULL,'5',3,NULL,'2018-10-19 12:59:26','192.168.10.13',1,1,'2018-09-19 13:57:26',1,'2018-09-26 09:13:54'),
(6,'030816','Sendiana','sendiana','1234',NULL,NULL,'5',3,NULL,'2018-09-26 09:19:21','192.168.10.38',0,1,'2018-09-26 09:16:05',1,'2020-04-16 13:59:22'),
(7,'1717','mobile','mrc@sgt','1234',NULL,'354584061184375','5',3,3,NULL,NULL,0,NULL,NULL,NULL,NULL);

/*Table structure for table `trn_detail` */

DROP TABLE IF EXISTS `trn_detail`;

CREATE TABLE `trn_detail` (
  `id_detail` int(5) NOT NULL AUTO_INCREMENT,
  `id_header` int(6) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `capaian` decimal(11,2) DEFAULT NULL,
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_detail` */

/*Table structure for table `trn_header` */

DROP TABLE IF EXISTS `trn_header`;

CREATE TABLE `trn_header` (
  `id_header` int(11) NOT NULL AUTO_INCREMENT,
  `periode_start` date DEFAULT NULL,
  `periode_end` date DEFAULT NULL,
  `goal` decimal(11,2) DEFAULT NULL,
  `capaian` decimal(11,2) DEFAULT NULL,
  `remarks` text,
  `persen` decimal(5,2) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_header`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_header` */

/*Table structure for table `trn_project_detail` */

DROP TABLE IF EXISTS `trn_project_detail`;

CREATE TABLE `trn_project_detail` (
  `project_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_header_id` int(11) DEFAULT NULL,
  `date_detail` date DEFAULT NULL,
  `remarks` text,
  `file_detail` text,
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_pic` varchar(50) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`project_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_project_detail` */

/*Table structure for table `trn_project_header` */

DROP TABLE IF EXISTS `trn_project_header`;

CREATE TABLE `trn_project_header` (
  `project_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_date` date DEFAULT NULL,
  `Customer` varchar(100) DEFAULT NULL,
  `project_type` varchar(100) DEFAULT NULL,
  `nominal` decimal(13,2) DEFAULT NULL,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(0) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`project_header_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_project_header` */

/*Table structure for table `trn_rab_detail` */

DROP TABLE IF EXISTS `trn_rab_detail`;

CREATE TABLE `trn_rab_detail` (
  `rab_detail_id` int(11) DEFAULT NULL,
  `rab_header_id` int(11) DEFAULT NULL,
  `items_name` varchar(50) DEFAULT NULL,
  `items_qty` decimal(12,2) DEFAULT NULL,
  `items_nominal` decimal(12,2) DEFAULT NULL,
  `nominal_qty` decimal(12,2) DEFAULT NULL COMMENT 'harga per items'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_rab_detail` */

/*Table structure for table `trn_rab_header` */

DROP TABLE IF EXISTS `trn_rab_header`;

CREATE TABLE `trn_rab_header` (
  `rab_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_no` varchar(15) DEFAULT NULL,
  `rab_date` date DEFAULT NULL,
  `rab_target` date DEFAULT NULL,
  `rab_project` varchar(150) DEFAULT NULL,
  `rab_remarks` varchar(150) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `input_pic` varchar(50) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`rab_header_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `trn_rab_header` */

insert  into `trn_rab_header`(`rab_header_id`,`rab_no`,`rab_date`,`rab_target`,`rab_project`,`rab_remarks`,`is_active`,`input_pic`,`input_time`,`edit_pic`,`edit_time`) values 
(1,'202108001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `trn_report_age` */

DROP TABLE IF EXISTS `trn_report_age`;

CREATE TABLE `trn_report_age` (
  `age_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_periode` date DEFAULT NULL,
  `a25_34` int(5) DEFAULT NULL,
  `a35_44` int(5) DEFAULT NULL,
  `a45_54` int(5) DEFAULT NULL,
  `a55_64` int(5) DEFAULT NULL,
  `a65_` int(5) DEFAULT NULL,
  `remarks` text,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`age_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_report_age` */

/*Table structure for table `trn_report_cs` */

DROP TABLE IF EXISTS `trn_report_cs`;

CREATE TABLE `trn_report_cs` (
  `id_cs` int(11) NOT NULL AUTO_INCREMENT,
  `date_periode` date DEFAULT NULL,
  `value_wa` decimal(11,2) DEFAULT NULL,
  `value_permintaan` decimal(11,2) DEFAULT NULL,
  `value_pembayaran` decimal(11,2) DEFAULT NULL,
  `remarks` text,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_data` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_report_cs` */

/*Table structure for table `trn_report_fb` */

DROP TABLE IF EXISTS `trn_report_fb`;

CREATE TABLE `trn_report_fb` (
  `id_fb` int(11) NOT NULL AUTO_INCREMENT,
  `date_periode` date DEFAULT NULL,
  `value_jangkauan` decimal(11,2) DEFAULT NULL,
  `value_klik` decimal(11,2) DEFAULT NULL,
  `value_install` decimal(11,2) DEFAULT NULL,
  `remarks` text,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_fb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_report_fb` */

/*Table structure for table `trn_report_gender` */

DROP TABLE IF EXISTS `trn_report_gender`;

CREATE TABLE `trn_report_gender` (
  `gender_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_periode` date DEFAULT NULL,
  `value_pria` int(8) DEFAULT NULL,
  `value_wanita` int(8) DEFAULT NULL,
  `remarks` text,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_report_gender` */

/*Table structure for table `trn_report_sem` */

DROP TABLE IF EXISTS `trn_report_sem`;

CREATE TABLE `trn_report_sem` (
  `id_sem` int(11) NOT NULL AUTO_INCREMENT,
  `date_periode` date DEFAULT NULL,
  `value_jangkauan` decimal(11,2) DEFAULT NULL,
  `value_klik` decimal(11,2) DEFAULT NULL,
  `value_install` decimal(11,2) DEFAULT NULL,
  `remarks` text,
  `is_active` int(2) DEFAULT '1',
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_sem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_report_sem` */

/*Table structure for table `trn_use_header` */

DROP TABLE IF EXISTS `trn_use_header`;

CREATE TABLE `trn_use_header` (
  `use_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_header_id` int(11) DEFAULT NULL,
  `use_date` date DEFAULT NULL,
  `use_remarks` varchar(150) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `input_pic` varchar(50) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `edit_pic` varchar(50) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`use_header_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_use_header` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
