<?php

namespace App\Controllers\Transaction; 

use CodeIgniter\Controller;
use App\Controllers\BaseController;
use App\Models\Transaction\Age_report_model;

class Age_report extends BaseController
{
	function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
    }

	public function index()
	{
		$trnHeader = new Age_report_model();

		$data['list_header'] 	= $trnHeader->getAll();
		$data['session'] 		= $this->session; 		
		return view("transaction/age_view",$data);
	}

    public function input()
	{
		return view("transaction/age_input");
	}

	function form_act(){
		$this->db = \Config\Database::connect('target', false);
		$ageReport = new Age_report_model();

		$this->db->transStart();

		$date_periode 		= $this->request->getVar('date_periode');
		$a25                = $this->request->getVar('a25');
		$a35     	        = $this->request->getVar('a35');
		$a45     	        = $this->request->getVar('a45');
		$a55     	        = $this->request->getVar('a55');
		$a65     	        = $this->request->getVar('a65');
		$remarks	        = $this->request->getVar('remarks');
        
     	$ageReport->setDatePeriode($date_periode);
     	$ageReport->setA2534($a25);
     	$ageReport->setA3544($a35);
     	$ageReport->setA4554($a45);
     	$ageReport->setA5564($a55);
     	$ageReport->setA65($a65);
     	$ageReport->setInputPic($this->session->get('set_session')['user_id']);
     	$ageReport->setInputTime(dbnow());
     	$save = $ageReport->ins();
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	public function edit($id)
	{
		$genderReport = new Age_report_model();

		$data['header']			= $genderReport->getById($id);

		return view("transaction/age_edit",$data);
	}

	function edit_act(){

        $this->db = \Config\Database::connect('target', false);
		$ageReport = new Age_report_model();

		$this->db->transStart();

		$date_periode 		= $this->request->getVar('date_periode');
		$a25                = $this->request->getVar('a25');
		$a35     	        = $this->request->getVar('a35');
		$a45     	        = $this->request->getVar('a45');
		$a55     	        = $this->request->getVar('a55');
		$a65     	        = $this->request->getVar('a65');
		$age_id	            = $this->request->getVar('age_id');
        
     	$ageReport->setDatePeriode($date_periode);
     	$ageReport->setA2534($a25);
     	$ageReport->setA3544($a35);
     	$ageReport->setA4554($a45);
     	$ageReport->setA5564($a55);
     	$ageReport->setA65($a65);
        $ageReport->setEditPic($this->session->get('set_session')['user_id']);
        $ageReport->setEditTime(dbnow());
        $save   = $ageReport->upd($age_id);
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }
		
	}

	function delete_js(){
		$v_id 			= $this->request->getVar('v_id');
		$trnHeader 		= new Age_report_model();

		return $trnHeader->update_status($v_id);
		
	}

}
