<?php namespace App\Controllers\transaction; 
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Aug 08, 2021                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
use CodeIgniter\Controller;
use App\Controllers\BaseController;
use App\Models\Transaction\Rab_header_model;
use App\Models\Transaction\Rab_detail_model;

class Rab extends BaseController
{

    function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
    }

	public function index()
	{
		$trnHeader = new Rab_header_model();

		$data['list_header'] 	= $trnHeader->getAll();
		$data['session'] 		= $this->session; 		
		return view("transaction/rab_view",$data);
	}

    public function input()
	{
		$this->session->removeTempdata('new_rab');
		$new_rab = $this->session->set('new_rab');

		if(!$new_rab){
            $new_rab = array(
                'items' => array(),
                'budget' => array()
            );
        }

		$this->session->set('new_rab', $new_rab);
		$data['session'] 		= $this->session; 	
		$data['new_rab']         = $new_rab;

		return view("transaction/rab_input",$data);
	}

    function add_item(){
		$item_id 		= $this->request->getVar('item_id');

		if(!isset($item_id)) return;
        $new_rab    = $this->session->get('new_rab');
        $items      = $new_rab['items'];

        $new_rab['items'][] = array(
            'item_id'       => $this->request->getVar('item_id'),
            'det_items'     => $this->request->getVar('det_items'),
            'det_qty'       => $this->request->getVar('det_qty'),
            'det_nominal'   => $this->request->getVar('det_nominal')
        );

        $exist = false;
        // test($new_rab,0);
        $this->session->set('new_rab', $new_rab);   
		
	}

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id 	= $this->request->getVar('index_id');
        $new_rab 	= $this->session->get('new_rab');

        $items = $new_rab['items'];
		
        foreach($items as $key=>$val){
            // test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                unset($new_rab['items'][$key]);
                $new_rab['items'] = array_values($new_rab['items']);
                break;
            }
        }
        // test($new_rab,0);
		$this->session->set('new_rab', $new_rab);   
		return json_encode(array('success'=>1));
    }

    function form_act(){
		$this->db = \Config\Database::connect('target', false);
		$trnHeader = new Rab_header_model();
		$trnDetail = new Rab_detail_model();

		$this->db->transStart();

		$table_id 		= $trnHeader->cek_id();
		$table_no 		= $trnHeader->cek_no($this->request->getVar('rab_date'));
		$new_rab        = $this->session->get('new_rab');

		$nominal_qty 	= 0;
		foreach($new_rab['items'] as $value){
            $nominal_qty 		= $value['det_nominal']/$value['det_qty'];
            $trnDetail->setRabHeaderId($table_id->rab_header_id);
            $trnDetail->setItemsName($value['det_items']);
            $trnDetail->setItemsQty($value['det_qty']);
            $trnDetail->setItemsNominal($value['det_nominal']);
            $trnDetail->setNominalQty($nominal_qty);
            $trnDetail->ins();
            
		}

        $trnHeader->setRabHeaderId($table_id->rab_header_id);
     	$trnHeader->setRabNo($table_no);
     	$trnHeader->setRabDate($this->request->getVar('rab_date'));
     	$trnHeader->setRabTarget($this->request->getVar('rab_target'));
     	$trnHeader->setRabProject($this->request->getVar('rab_project'));
     	$trnHeader->setRabRemarks($this->request->getVar('remarks'));
     	$trnHeader->setIsActive('1');
     	$trnHeader->setInputPic($this->session->get('set_session')['user_id']);
     	$trnHeader->setInputTime(dbnow());

     	$save   = $trnHeader->ins();
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $table_no));
        }

	}

    public function edit($id){
		$this->db = \Config\Database::connect('target', false);
		$trnHeader = new Rab_header_model();
		$trnDetail = new Rab_detail_model();

		$this->session->removeTempdata('new_rab');
		$new_rab = $this->session->set('new_rab');

		$detail 		= $trnDetail->getByIdHeader($id)->getResultArray();
		$tdetail 		= $trnDetail->getByIdHeader($id)->getNumRows();

		if($tdetail==0){
            $new_rab = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                $new_rab['items'][$key] = array(
                    'item_id'       => $val['rab_detail_id'],
                    'det_items'     => $val['items_name'],
                    'det_qty'       => $val['items_qty'],
                    'det_nominal'   => $val['items_nominal']
                );
            }
        }
		$this->session->set('new_rab', $new_rab);
		$data['session'] 		= $this->session; 	
		$data['new_rab']        = $new_rab;
		$data['header']			= $trnHeader->getById($id);

		return view("transaction/rab_edit",$data);
	}

    function edit_act(){
		$this->db = \Config\Database::connect('target', false);
		$trnHeader = new Rab_header_model();
		$trnDetail = new Rab_detail_model();

		$this->db->transStart();

		$rab_id 		= $this->request->getVar('rab_id');
		$new_rab    	= $this->session->get('new_rab');
		$del_detail 	= $trnDetail->del_detail($rab_id);

		$nominal_qty 	= 0;
		foreach($new_rab['items'] as $value){
			// test($value,1);
			$nominal_qty 		= $value['det_nominal']/$value['det_qty'];
            $trnDetail->setRabHeaderId($rab_id);
            $trnDetail->setItemsName($value['det_items']);
            $trnDetail->setItemsQty($value['det_qty']);
            $trnDetail->setItemsNominal($value['det_nominal']);
            $trnDetail->setNominalQty($nominal_qty);
            $trnDetail->ins();

		}
		
        $trnHeader->setRabDate($this->request->getVar('rab_date'));
        $trnHeader->setRabTarget($this->request->getVar('rab_target'));
        $trnHeader->setRabProject($this->request->getVar('rab_project'));
        $trnHeader->setRabRemarks($this->request->getVar('remarks'));
        $trnHeader->setIsActive('1');
        $trnHeader->setEditPic($this->session->get('set_session')['user_id']);
        $trnHeader->setEditTime(dbnow());

     	$save   = $trnHeader->upd($rab_id);
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

    function detail_items_rab(){
        $id         = $this->request->getVar('id');
        $rabDetail = new Rab_detail_model();
        $result     = $rabDetail->getByIdHeader($id)->getResult();
        // test($result,1);
        echo json_encode($result);
    }

    function delete_js(){
		$v_id 			= $this->request->getVar('v_id');
		$trnHeader 		= new Rab_header_model();

		return $trnHeader->update_status($v_id);
		
	}

    function report(){
        $trnRab = new Rab_header_model();

        $data['search'] 		= $this->request->getVar('search');
        $data['data_project']   = $trnRab->getAll();

        if($this->request->getVar('search')!=''){
            $data['rab_header']   = $trnRab->getByIdRow($this->request->getVar('rab_id'));
            $data['rab_detail']   = $trnRab->search_project($this->request->getVar('rab_id'));
        }
        
        return view('transaction/rab_report',$data);
    }























}
?>
