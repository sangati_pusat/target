<?php

namespace App\Controllers\Transaction; 

use CodeIgniter\Controller;
use App\Controllers\BaseController;
use App\Models\Transaction\Gender_report_model;

class Gender_report extends BaseController
{
	function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
    }

	public function index()
	{
		$trnHeader = new Gender_report_model();

		$data['list_header'] 	= $trnHeader->getAll();
		$data['session'] 		= $this->session; 		
		return view("transaction/gender_view",$data);
	}

    public function input()
	{
		return view("transaction/gender_input");
	}

	function form_act(){
		$this->db = \Config\Database::connect('target', false);
		$genderReport = new Gender_report_model();

		$this->db->transStart();

		$date_periode 		= $this->request->getVar('date_periode');
		$nilai_perempuan    = $this->request->getVar('nilai_perempuan');
		$nilai_laki     	= $this->request->getVar('nilai_laki');
		$remarks	        = $this->request->getVar('remarks');
        
     	$genderReport->setDatePeriode($date_periode);
     	$genderReport->setValuePria($nilai_laki);
     	$genderReport->setValueWanita($nilai_perempuan);
     	$genderReport->setRemarks($remarks);
     	$genderReport->setInputPic($this->session->get('set_session')['user_id']);
     	$genderReport->setInputTime(dbnow());
     	$save   = $genderReport->ins();
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	public function edit($id)
	{
		$genderReport = new Gender_report_model();

		$data['header']			= $genderReport->getById($id);

		return view("transaction/gender_edit",$data);
	}

	function edit_act(){

        $this->db = \Config\Database::connect('target', false);
		$genderReport = new Gender_report_model();

		$this->db->transStart();

		$date_periode 		= $this->request->getVar('date_periode');
		$nilai_perempuan    = $this->request->getVar('nilai_perempuan');
		$nilai_laki     	= $this->request->getVar('nilai_laki');
		$remarks	        = $this->request->getVar('remarks');
        $gender_id          = $this->request->getVar('gender_id');
        
     	$genderReport->setDatePeriode($date_periode);
     	$genderReport->setValuePria($nilai_laki);
     	$genderReport->setValueWanita($nilai_perempuan);
     	$genderReport->setRemarks($remarks);
     	$genderReport->setEditPic($this->session->get('set_session')['user_id']);
     	$genderReport->setEditTime(dbnow());
     	$save   = $genderReport->upd($gender_id);
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }
		
	}

	function delete_js(){
		$v_id 			= $this->request->getVar('v_id');
		$trnHeader 		= new Gender_report_model();

		return $trnHeader->update_status($v_id);
		
	}

}
