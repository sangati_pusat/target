<?php

namespace App\Controllers\Transaction; 

use CodeIgniter\Controller;
use App\Controllers\BaseController;
use App\Models\Transaction\Cs_report_model;

class Cs_target extends BaseController
{
	function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
    }

	public function index()
	{
		$trnHeader = new Cs_report_model();

		$data['list_header'] 	= $trnHeader->getAll();
		$data['session'] 		= $this->session; 		
		return view("transaction/cs_view",$data);
	}

    public function input()
	{
		return view("transaction/cs_input");
	}

	function form_act(){
		$this->db = \Config\Database::connect('target', false);
		$csReport = new Cs_report_model();

		$this->db->transStart();

		$date_periode 			= $this->request->getVar('date_periode');
		$nilai_wa 				= $this->request->getVar('nilai_whatsapp');
		$nilai_permintaan	    = $this->request->getVar('nilai_permintaan');
		$nilai_pembayaran	    = $this->request->getVar('nilai_pembayaran');
        
     	$csReport->setDatePeriode($date_periode);
     	$csReport->setValueWa($nilai_wa);
     	$csReport->setValuePermintaan($nilai_permintaan);
     	$csReport->setValuePembayaran($nilai_pembayaran);
     	$csReport->setInputPic($this->session->get('set_session')['user_id']);
     	$csReport->setInputDate(dbnow());
     	$save   = $csReport->ins();
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	public function edit($id)
	{
		$csReport = new Cs_report_model();

		$data['header']			= $csReport->getById($id);

		return view("transaction/cs_edit",$data);
	}

	function edit_act(){
		
        $this->db = \Config\Database::connect('target', false);
		$csReport = new Cs_report_model();

		$this->db->transStart();

		$date_periode 			= $this->request->getVar('date_periode');
		$nilai_wa 				= $this->request->getVar('nilai_whatsapp');
		$nilai_permintaan	    = $this->request->getVar('nilai_permintaan');
		$nilai_pembayaran	    = $this->request->getVar('nilai_pembayaran');
		$id_cs          	    = $this->request->getVar('id_cs');
        
        
     	$csReport->setDatePeriode($date_periode);
     	$csReport->setValueWa($nilai_wa);
     	$csReport->setValuePermintaan($nilai_permintaan);
     	$csReport->setValuePembayaran($nilai_pembayaran);
     	$csReport->setEditPic($this->session->get('set_session')['user_id']);
     	$csReport->setEditData(dbnow());
     	$save   = $csReport->upd($id_cs);
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	function delete_js(){
		$v_id 			= $this->request->getVar('v_id');
		$trnHeader 		= new Cs_report_model();

		return $trnHeader->update_status($v_id);
		
	}

}
