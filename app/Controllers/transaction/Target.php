<?php

namespace App\Controllers\Transaction; 

use CodeIgniter\Controller;
use App\Controllers\BaseController;
use App\Models\Transaction\Target_model_01;
use App\Models\Transaction\Target_model_02;

class Target extends BaseController
{
	function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
    }

	public function index()
	{
		$trnHeader = new Target_model_01();

		$data['list_header'] 	= $trnHeader->getAll();
		$data['session'] 		= $this->session; 		
		return view("transaction/target_view",$data);
	}

    public function input()
	{
		$this->session->removeTempdata('new_pr');
		$new_pr = $this->session->set('new_pr');

		if(!$new_pr){
            $new_pr = array(
                'items' => array(),
                'budget' => array()
            );
        }

		$this->session->set('new_pr', $new_pr);
		$data['session'] 		= $this->session; 	
		$data['new_pr']         = $new_pr;

		return view("transaction/target_input",$data);
	}

	function add_item(){
		$item_id 		= $this->request->getVar('item_id');

		if(!isset($item_id)) return;
        $new_pr    = $this->session->get('new_pr');
        $items      = $new_pr['items'];

        $new_pr['items'][] = array(
            'item_id'       => $this->request->getVar('item_id'),
            'item_name'     => $this->request->getVar('tgl_target'),
            'item_qty'      => $this->request->getVar('capaian'),
            'item_info'     => $this->request->getVar('item_info')
        );

        $exist = false;
        // test($new_pr,0);
        $this->session->set('new_pr', $new_pr);   
		
	}

	function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id 	= $this->request->getVar('index_id');
        $new_pr 	= $this->session->get('new_pr');

        $items = $new_pr['items'];
		
        foreach($items as $key=>$val){
            test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                unset($new_pr['items'][$key]);
                $new_pr['items'] = array_values($new_pr['items']);
                break;
            }
        }
        // test($new_pr,0);
		$this->session->set('new_pr', $new_pr);   
		return json_encode(array('success'=>1));
    }

	function form_act(){
		$this->db = \Config\Database::connect('target', false);
		$trnHeader = new Target_model_01();
		$trnDetail = new Target_model_02();

		$this->db->transStart();

		$start_date 			= $this->request->getVar('start_date');
		$end_date 				= $this->request->getVar('end_date');
		$goal 					= $this->request->getVar('goal');
		$target 				= $this->request->getVar('target');
		$remarks 				= $this->request->getVar('remarks');


		$table_id 		= $trnHeader->cek_id();

		$new_pr    = $this->session->get('new_pr');

		$total_capaian 	= 0;
		foreach($new_pr['items'] as $value){
			// test($value,1);
			$trnDetail->setIdHeader($table_id->id_header);
			$trnDetail->setTanggal($value['item_name']);
			$trnDetail->setCapaian($value['item_qty']);
			$trnDetail->setInputPic($this->session->get('set_session')['user_id']);
			$trnDetail->setInputDate(dbnow());
			$trnDetail->ins();

			$total_capaian 		= $total_capaian+$value['item_qty'];
		}

		$persen 		= ($total_capaian/$goal)*100;
		
     	$trnHeader->setIdHeader($table_id->id_header);
     	$trnHeader->setPeriodeStart($start_date);
     	$trnHeader->setPeriodeEnd($end_date);
     	$trnHeader->setGoal($goal);
     	$trnHeader->setCapaian($total_capaian);
     	$trnHeader->setRemarks($remarks);
     	$trnHeader->setPersen($persen);
     	$trnHeader->setType($target);
     	$trnHeader->setInputPic($this->session->get('set_session')['user_id']);
     	$trnHeader->setInputDate(dbnow());
     	$save   = $trnHeader->ins();
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	public function edit($id)
	{
		$trnHeader = new Target_model_01();
		$trnDetail = new Target_model_02();

		$this->session->removeTempdata('new_pr');
		$new_pr = $this->session->set('new_pr');

		$detail 		= $trnDetail->getByIdHeader($id)->getResultArray();
		$tdetail 		= $trnDetail->getByIdHeader($id)->getNumRows();

		if($tdetail==0){
            $new_pr = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                $new_pr['items'][$key] = array(
                    'item_id'       => $val['id_detail'],
                    'item_name'    	=> $val['tanggal'],
                    'item_qty'      => $val['capaian'],
                    'item_info'     => ''
                );
            }
        }
		$this->session->set('new_pr', $new_pr);
		$data['session'] 		= $this->session; 	
		$data['new_pr']         = $new_pr;
		$data['header']			= $trnHeader->getById($id);

		return view("transaction/target_edit",$data);
	}

	function edit_act(){
		$this->db = \Config\Database::connect('target', false);
		$trnHeader = new Target_model_01();
		$trnDetail = new Target_model_02();

		$this->db->transStart();

		$start_date 			= $this->request->getVar('start_date');
		$end_date 				= $this->request->getVar('end_date');
		$goal 					= $this->request->getVar('goal');
		$target 				= $this->request->getVar('target');
		$remarks 				= $this->request->getVar('remarks');
		$id_header 				= $this->request->getVar('id_header');


		$table_id 		= $trnHeader->cek_id();

		$new_pr    		= $this->session->get('new_pr');

		$total_capaian 	= 0;
		$del_detail 	= $trnDetail->del_detail($id_header);
		// test($new_pr,1);
		foreach($new_pr['items'] as $value){
			// test($value,1);
			$trnDetail->setIdHeader($id_header);
			$trnDetail->setTanggal($value['item_name']);
			$trnDetail->setCapaian($value['item_qty']);
			$trnDetail->setInputPic($this->session->get('set_session')['user_id']);
			$trnDetail->setInputDate(dbnow());
			$trnDetail->ins();

			$total_capaian 		= $total_capaian+$value['item_qty'];
		}
		// test($total_capaian.' < - > '.$goal,1);
		$persen 		= ($total_capaian/$goal)*100;
		
     	$trnHeader->setPeriodeStart($start_date);
     	$trnHeader->setPeriodeEnd($end_date);
     	$trnHeader->setGoal($goal);
     	$trnHeader->setCapaian($total_capaian);
     	$trnHeader->setRemarks($remarks);
     	$trnHeader->setPersen($persen);
     	$trnHeader->setType($target);
     	$trnHeader->setInputPic($this->session->get('set_session')['user_id']);
     	$trnHeader->setInputDate(dbnow());
     	$save   = $trnHeader->upd($id_header);
		
		$this->db->transComplete();

        if ($this->db->transStatus() === FALSE){
            $this->db->transRollback();
			return json_encode(array('success' => false));
        }else{
			$this->db->transCommit();
			return json_encode(array('success' => true, 'status' => $save));
        }

	}

	function delete_js(){
		$v_id 			= $this->request->getVar('v_id');
		$trnHeader 		= new Target_model_01();

		return $trnHeader->update_status($v_id);
		
	}

}
