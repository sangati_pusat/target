<?php namespace App\Controllers;

use \App\Models\Master\UsersModel;
use \App\Models\Admin\MenuModel;

class Users extends BaseController
{
	public function __construct()
	{
		helper(['url', 'form', 'security']);
		
	}

	public function index()
	{
		$session = \Config\Services::session();

		if(!isset($session->get('set_session')['name'])){
			if($this->request->getVar('username')!==null){

				$usersModel 	= new UsersModel();
				$username 		= $this->request->getVar('username');
				$password 		= $this->request->getVar('password');

				$data 			= $usersModel->login_app($username,$password)->getNumRows();

				if($data>=1){
					$row 			= $usersModel->login_app($username,$password)->getRow();

					// $session = session();					
					$dSession 	= array(
							'user_id'      => $row->user_id,
							'nip'          => $row->nip,
							'name'         => $row->name,
							'username'	   => $row->username,
							'menu'		   => $usersModel->menu_permission($row->user_id)->getResult(),
							'submenu'	   => $usersModel->submenu_permission($row->user_id)->getResult()
						);

					$session->set('set_session', $dSession);
					return redirect()->to('home');

				}else{
					return view('login');
				}
				
			}else{
				return view('login');
			}
		}else{
			return redirect()->to('home');			
		}
	}

	public function logout(){
		$session = session();
		$session->destroy();
		return view('login');
	}
	


	// public function act_login(){
	// 	dd($this->request->getVar());
	// }
}
