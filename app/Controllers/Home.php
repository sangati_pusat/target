<?php

namespace App\Controllers;
use App\Models\Transaction\Target_model_01;
use App\Models\Transaction\Cs_report_model;
use App\Models\Transaction\Gender_report_model;
use App\Models\Transaction\Age_report_model;
use App\Models\Transaction\Sem_report_model;
use App\Models\Transaction\Fb_report_model;
	
class Home extends BaseController
{
	function __construct(){
        $session = session();
		$this->session = \Config\Services::session();
		helper('common');
		
    }

	public function index()
	{
		$trnHeader 				= new Target_model_01();
		$trnCs 					= new Cs_report_model();
		$trnGender				= new Gender_report_model();
		$trnAge					= new Age_report_model();
		$trnSem					= new Sem_report_model();
		$trnFb					= new Fb_report_model();

		$data['search'] 		= $this->request->getVar('search');
		$data['session'] 		= $this->session; 

		// test($this->request->getVar('search'),1);

		if($this->request->getVar('search')!=''){
			$tgl_awal	= $this->request->getVar('date_awal');
			$tgl_akhir	= $this->request->getVar('date_akhir');

			// $data['session'] 		= $this->session; 
			$data['tanggal_awal']	= $this->request->getVar('date_awal');
			$data['tanggal_akhir']	= $this->request->getVar('date_akhir');
			$data['header'] 		= array(
				'download'		=> $trnHeader->dashboard_header('Download',$tgl_awal,$tgl_akhir),
				'pendapatan'	=> $trnHeader->dashboard_header('Pendapatan',$tgl_awal,$tgl_akhir)
			);
			// test($data['header'],1);
			$data['tgl_cs']			= $trnCs->tgl_cs($tgl_awal,$tgl_akhir);
			$data['value_cs']		= $trnCs->value_cs($tgl_awal,$tgl_akhir);

			$data['value_gender']	= $trnGender->value_gender($tgl_awal,$tgl_akhir);
			$data['value_age']		= $trnAge->value_age($tgl_awal,$tgl_akhir);

			$data['tgl_sem']		= $trnSem->tgl_sem($tgl_awal,$tgl_akhir);
			$data['value_sem']		= $trnSem->value_sem($tgl_awal,$tgl_akhir);

			$data['tgl_fb']			= $trnFb->tgl_fb($tgl_awal,$tgl_akhir);
			$data['value_fb']		= $trnFb->value_fb($tgl_awal,$tgl_akhir);
			// test($data,1);
		}else{
			$data['tanggal_awal']	= dbnow();
			$data['tanggal_akhir']	= date('Y-m-d', strtotime('+3 month', strtotime(dbnow())));

			$data['header'] 		= array(
				'download'		=> array(
					'goal' => '',
		            'capaian' => '',
		            'persen' => ''),
				'pendapatan'	=> array(
					'goal' => '',
		            'capaian' => '',
		            'persen' => '')
			);
			$data['tgl_cs']			= '';
			$data['value_cs']		= '';

			$data['value_gender']	= '';
			$data['value_age']		= '';

			$data['tgl_sem']		= '';
			$data['value_sem']		= '';

			$data['tgl_fb']			= '';
			$data['value_fb']		= '';
			
		}
		

		return view('welcome_message',$data);
	}
}
