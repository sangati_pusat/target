<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="breadcrumbs">
	<div class="breadcrumbs-inner">
		<div class="row m-0">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Customer Service</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<!-- <li class="active"><a class="btn btn-outline-primary btn-sm" href="<?= base_url('transaction/input') ?>">Input</a></li> -->
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Input Report Customer Service</strong>
                    </div>
                    <div class="card-body card-block">
                        <!-- <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Tanggal</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="date" id="date_periode" name="date_periode" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Jenis</label></div>
                                <div class="col-12 col-md-5">
                                    <div class="form-check-inline form-check">
                                        <label for="inline-radio1" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Whatsapp" class="form-check-input">Whatsapp
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Permintaan" class="form-check-input">Permintaan
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Pembayaran" class="form-check-input">Pembayaran
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Whatsapp</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="email" id="nilai_whatsapp" name="nilai" placeholder="Jumlah Whatsapp" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Permintaan</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="email" id="nilai_permintaan" name="nilai" placeholder="Jumlah Permintaan" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Pembayaran</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="email" id="nilai_pembayaran" name="nilai" placeholder="Jumlah Pembayaran" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="textarea-input" class=" form-control-label">Remarks</label></div>
                                <div class="col-12 col-md-9"><textarea name="remarks" id="remarks" rows="5" placeholder="Remarks..." class="form-control"></textarea></div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-info btn-sm" id="save"> Save </button>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('transaction/target') ?>">Cancel</a>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    $("#start_date").focus();

    pr = {
        data: {},
        processed: false,
        items: [],

        init: function(){
            $('#save').click(pr.save);
        },

        save: function(e){
            e.preventDefault();
            
            if($('#date_periode').val()==''){
                toastr.error("<strong>Periode</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#date_periode').focus();
                return false;
            }

            $.ajax({
                url: '../cs_target/form_act',
                type : "POST",  
                dataType : 'json', // Jika ini digunakan, tidap perlu pakai $.parseJSON();
                data: {
                    date_periode    : $('#date_periode').val(),
                    nilai_whatsapp  : $('#nilai_whatsapp').val(),
                    nilai_permintaan: $('#nilai_permintaan').val(),
                    nilai_pembayaran: $('#nilai_pembayaran').val(),
                    remarks         : $('#remarks').val(),
                },
                success : function(resp){
                    // var myObj = $.parseJSON(resp); 
                    var myObj = resp;
                    if(myObj.status == 'ERROR INSERT' || myObj.status == false) {
                        toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
                        return false;
                    } else {
                        messages = 'Data Berhasil di Simpan ';
                        messages += "<hr>";

                        toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
                        
                        setTimeout(function () {
                            window.location.href = '../cs_target/'; 
                        }, 2000);
                    }
                }
            });

            $('#save').prop("disabled",true);

        }

    };

    pr.init();

</script>
<?= $this->endSection() ?>