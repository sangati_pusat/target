<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="breadcrumbs">
	<div class="breadcrumbs-inner">
		<div class="row m-0">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Rencana Anggaran Biaya</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<!-- <li class="active"><a class="btn btn-outline-primary btn-sm" href="<?= base_url('transaction/input') ?>">Input</a></li> -->
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Input RAB</strong>
                    </div>
                    <div class="card-body card-block">
                        <!-- <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Tanggal</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="date" id="rab_date" name="rab_date" class="form-control" value="<?= $header['rab_date']; ?>">
                                    <input type="hidden" id="rab_id" name="rab_date" class="form-control" value="<?= $header['rab_header_id']; ?>">
                                    <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_rab["items"]); ?>' required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Target</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="date" id="rab_target" name="rab_target" placeholder="Text" class="form-control" value="<?= $header['rab_target']; ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Project</label></div>
                                <div class="col-12 col-md-8">
                                    <input type="text" id="rab_project" name="target_date" placeholder="Text" class="form-control" value="<?= $header['rab_project']; ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="textarea-input" class=" form-control-label">Remarks</label></div>
                                <div class="col-12 col-md-9"><textarea name="remarks" id="remarks" rows="5" placeholder="Remarks..." class="form-control"><?= $header['rab_remarks'] ?></textarea></div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Nama Barang</label></div>
                                <div class="col-12 col-md-6">
                                    <input type="text" id="det_items" name="det_items" placeholder="Nama Barang " class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Quantity</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="det_qty" name="det_qty" placeholder="Qty" class="form-control">
                                    <input type="hidden" class="form-control " id="id" name="id" value="0"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Nominal</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="text" id="det_nominal" name="det_nominal" placeholder="Nominal" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label"></label></div>
                                <div class="col-12 col-md-3">
                                    <button type="submit" class="btn btn-primary btn-sm" id="add-items"> Add Detail</button>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered" id="detail">
                                            <thead>
                                                <tr>
                                                    <th width="1%">ID</th>
                                                    <th>Nama Barang</th>
                                                    <th width="20%">Quantity</th>
                                                    <th width="20%">Nominal</th>
                                                    <th width="5%">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info btn-sm" id="save"> Save </button>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('transaction/rab') ?>">Cancel</a>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    $("#start_date").focus();

    pr = {
        data: {},
        processed: false,
        items: [],

        init: function(){
            this.grids = $('#detail').DataTable({
                "paging": false, 
                "bLengthChange": false, // disable show entries dan page
                "bFilter": false,
                "bInfo": false, // disable Showing 0 to 0 of 0 entries
                "bAutoWidth": false,
                "language": {
                    "emptyTable": "Tidak Ada Data"
                },
                "columnDefs": [
                    { // disable sorting on column process buttons
                    "targets": [1,2,3,4],
                    "orderable": false,
                    },
                    { 
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ],
                columns: [
                    { data: 'item_id'},
                    { data: 'det_items'}, 
                    { data: 'det_qty', className: "text-right"}, 
                    { data: 'det_nominal',className: "text-right"}, 
                    { data: 'act', className: "text-center" }
                ],
            });

            this._set_items($('#sup_items').val());
            $('#add-items').click(pr.add_items);
            $('#save').click(pr.save);
        },

        _set_items: function(items){
            this.no_ajax = true;
            //
            if(items) items = JSON.parse(items);
            this.items = items;
            items.map(function(i,e){
                var data = {
                    item_id     : i.item_id,
                    det_items   : i.det_items,
                    det_qty     : i.det_qty,
                    det_nominal : i.det_nominal
                };
                pr._addtogrid(data);
                pr._focus();
                pr._clearitem();
            });
            this.no_ajax = false;

        },

        _focus: function(){
            $('#det_items').focus();
        },

        _clearitem: function(){
            $('#det_items').val('');
            $('#det_qty').val('');
            $('#det_nominal').val('');
        },

        add_items: function (e) {
            e.preventDefault();
            if($('#det_items').val()==''){
                toastr.error("<strong>Nama Items</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#det_items').focus();
                return false;
            }

            if($('#det_qty').val()==''){
                toastr.error("<strong>Quantity</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#det_qty').focus();
                return false;
            }
            
            if($('#det_qty').val()<=0 || $('#det_qty').val()==''){
                toastr.error("<strong>Quantity</strong> Tidak Boleh Bernilai <strong>Nol ( 0 )</strong>", 'Alert', {"positionClass": "toast-top-center"});
                $('#det_qty').focus();
                return false;
            }

            if(!$('#det_nominal').val()){
                toastr.error("<strong>Nominal</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $("#det_nominal").focus();
                return false;
            }             

            if($('#det_nominal').val()<=0 || $('#det_nominal').val()==''){
                toastr.error("<strong>Nominal</strong> Tidak Boleh Bernilai <strong>Nol ( 0 )</strong>", 'Alert', {"positionClass": "toast-top-center"});
                $("#det_nominal").focus();
                return false;
            } 

            let det_items   = $('#det_items').val();
            let det_qty     = $('#det_qty').val();
            let det_nominal = $('#det_nominal').val();
            let id          = parseInt($('#id').val());
            var item_id     = id + 1;

            if(item_id){
                data = {
                    item_id     : item_id,
                    det_items   : det_items,
                    det_qty     : det_qty,
                    det_nominal : det_nominal
                };

                pr._addtogrid(data);
                pr._clearitem();
            }

        },

        _addtogrid: function(data){
            let grids = this.grids;
            let exist = pr.grids.row('#'+data.item_id).index();
            //
            $('#id').val(data.item_id);

            data.act = '<button class="btn btn-outline-warning btn-sm" det-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);"><strong>X</strong></button>';
            data.DT_RowId = data.item_id;
            //
            if(exist===undefined){
                grids.row.add(data).draw(); 
            }else{ 
                toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
                return false;
            }

            if(this.no_ajax) return false;

            var myUrl ="../../rab/add_item";
            $.ajax({
                url    : myUrl,
                method : "POST",
                data: {
                    item_id     : data.item_id,
                    det_items   : data.det_items,
                    det_qty     : data.det_qty,
                    det_nominal : data.det_nominal
                }
            });
            
            pr._focus();
        },

        _removefromgrid: function(el){
            let id = $(el).attr('det-id');
            pr.grids.row("#"+id).remove().draw();
            $.ajax({
                method: "GET",
                url: '../../rab/remove_item',
                data: {
                    index_id: id
                }
            });
            return false;
        },

        _focus: function(){
            $('#tgl_target').focus();
        },

        save: function(e){
            e.preventDefault();
            
            if($('#rab_date').val()==''){
                toastr.error("<strong>Tanggal </strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#start_date').focus();
                return false;
            }

            if($('#rab_target').val()==''){
                toastr.error("<strong>Target</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#rab_target').focus();
                return false;
            }

            if($('#rab_project').val()==''){
                toastr.error("<strong>Project</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#rab_project').focus();
                return false;
            }

            $.ajax({
                url: '../../rab/edit_act',
                type : "POST",  
                dataType : 'json', // Jika ini digunakan, tidap perlu pakai $.parseJSON();
                data: {
                    rab_id          : $('#rab_id').val(),
                    rab_date        : $('#rab_date').val(),
                    rab_target      : $('#rab_target').val(),
                    rab_project     : $('#rab_project').val(),
                    remarks         : $('#remarks').val()
                },
                success : function(resp){
                    // var myObj = $.parseJSON(resp); 
                    var myObj = resp;
                    if(myObj.status == 'ERROR INSERT' || myObj.status == false) {
                        toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
                        return false;
                    } else {
                        messages = 'Data Berhasil di Update';
                        messages += "<hr>";

                        toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
                        
                        setTimeout(function () {
                            window.location.href = '../../rab/'; 
                        }, 2000);
                    }
                }
            });

            $('#save').prop("disabled",true);

        }



    };

    pr.init();

</script>
<?= $this->endSection() ?>