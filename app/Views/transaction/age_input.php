<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="breadcrumbs">
	<div class="breadcrumbs-inner">
		<div class="row m-0">
			<div class="col-sm-9">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Age Report </h1>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<!-- <li class="active"><a class="btn btn-outline-primary btn-sm" href="<?= base_url('transaction/gender_report/input') ?>">Input</a></li> -->
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Input Report Age</strong>
                    </div>
                    <div class="card-body card-block">
                        <!-- <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Tanggal</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="date" id="date_periode" name="date_periode" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Jenis</label></div>
                                <div class="col-12 col-md-5">
                                    <div class="form-check-inline form-check">
                                        <label for="inline-radio1" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Whatsapp" class="form-check-input">Whatsapp
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Permintaan" class="form-check-input">Permintaan
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Pembayaran" class="form-check-input">Pembayaran
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Umur 25 - 34</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="a25" name="nilai" placeholder="Umur 25 - 34" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Umur 35 - 44</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="a35" name="nilai" placeholder="Umur 35 - 44" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Umur 45 - 54</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="a45" name="nilai" placeholder="Umur 45 - 54" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Umur 55 - 64</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="a55" name="nilai" placeholder="Umur 55 - 64" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Umur 65 ++</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="text" id="a65" name="nilai" placeholder="Umur 65 ++" class="form-control">
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="textarea-input" class=" form-control-label">Remarks</label></div>
                                <div class="col-12 col-md-9"><textarea name="remarks" id="remarks" rows="5" placeholder="Remarks..." class="form-control"></textarea></div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-info btn-sm" id="save"> Save </button>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('transaction/age_report') ?>">Cancel</a>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    $("#start_date").focus();

    pr = {
        data: {},
        processed: false,
        items: [],

        init: function(){
            $('#save').click(pr.save);
        },

        save: function(e){
            e.preventDefault();
            
            if($('#date_periode').val()==''){
                toastr.error("<strong>Periode</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#date_periode').focus();
                return false;
            }

            $.ajax({
                url: '../age_report/form_act',
                type : "POST",  
                dataType : 'json', // Jika ini digunakan, tidap perlu pakai $.parseJSON();
                data: {
                    date_periode    : $('#date_periode').val(),
                    a25             : $('#a25').val(),
                    a35             : $('#a35').val(),
                    a45             : $('#a45').val(),
                    a55             : $('#a55').val(),
                    a65             : $('#a65').val(),
                    remarks         : $('#remarks').val(),
                },
                success : function(resp){
                    // var myObj = $.parseJSON(resp); 
                    var myObj = resp;
                    if(myObj.status == 'ERROR INSERT' || myObj.status == false) {
                        toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
                        return false;
                    } else {
                        messages = 'Data Berhasil di Simpan ';
                        messages += "<hr>";

                        toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
                        
                        // setTimeout(function () {
                        //     window.location.href = '../age_report/'; 
                        // }, 2000);
                    }
                }
            });

            $('#save').prop("disabled",true);

        }

    };

    pr.init();

</script>
<?= $this->endSection() ?>