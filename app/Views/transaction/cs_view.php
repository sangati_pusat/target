<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="breadcrumbs">
	<div class="breadcrumbs-inner">
		<div class="row m-0">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Report CS</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<li class="active"><a class="btn btn-outline-primary btn-sm" href="<?= base_url('transaction/cs_target/input') ?>">Input</a></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">View Report CS</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Tanggal</th>
                                    <th colspan="3">Nilai</th>
                                    <th rowspan="2">Remarks</th>
                                    <th rowspan="2">Action</th>
                                </tr>
                                <tr>
                                    <th>WA</th>
                                    <th>Permintaan</th>
                                    <th>Pembayaran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0; 
                                foreach($list_header as $value){
                                $no         = $no+1;
                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= tgl_singkat($value['date_periode']); ?></td>
                                    <td align="right"><?= money_dec($value['value_wa'],2) ?></td>
                                    <td align="right"><?= money_dec($value['value_permintaan'],2) ?></td>
                                    <td align="right"><?= money_dec($value['value_pembayaran'],2) ?></td>
                                    <td><?= $value['remarks'] ?></td>
                                    <td align="center">
                                        <a class="btn btn-warning btn-sm" href="../transaction/cs_target/edit/<?= $value['id_cs']; ?>">Edit</a>
                                        <button class="btn btn-danger btn-sm" id="delete" data-id='<?= $value['id_cs']; ?>'> Hapus </button>
                                    </td>
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


<div class="clearfix"></div>

<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
    <script>
        $('#bootstrap-data-table').DataTable();

        $('#bootstrap-data-table').on('click','#delete', function(e){
            var v_id  = $(this).data('id');

            toastr.warning(
                'Apakah Ingin Menghapus ?<br /><br />'+
                '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-v_id="'+v_id+'">Yes</button> '+
                '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
                '<u>ALERT</u>', 
                {
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "closeButton": false,
                }
            );
        });

        function deleteRow(e){
            var v_id  = $(e).data('v_id');

            $.ajax({
                data: {
                    v_id  : v_id
                },
                type : "POST",
                url: '<?php echo base_url(); ?>/transaction/cs_target/delete_js',
                success : function(resp){

                    if(resp.status == 'ERROR INSERT' || resp.status == false) {
                        alert('Data Tidak berhasil di Hapus');
                        return false;

                    } else {
                        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

                        setTimeout(function () {
                        window.location.href = '<?php echo base_url(); ?>/transaction/cs_target/'; //will redirect to google.
                        }, 2000);
                    }
                }
            });
        }
    </script>
<?= $this->endSection() ?>