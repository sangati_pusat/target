<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="breadcrumbs">
	<div class="breadcrumbs-inner">
		<div class="row m-0">
			<div class="col-sm-4">
				<div class="page-header float-left">
					<div class="page-title">
						<h1>Target</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="page-header float-right">
					<div class="page-title">
						<ol class="breadcrumb text-right">
							<!-- <li class="active"><a class="btn btn-outline-primary btn-sm" href="<?= base_url('transaction/input') ?>">Input</a></li> -->
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Input Target</strong>
                    </div>
                    <div class="card-body card-block">
                        <!-- <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="text-input" class=" form-control-label">Periode</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="date" id="start_date" name="start_date" class="form-control">
                                </div>
                                <div class="col-12 col-md-1" style="padding: 8px 0px 10px 35px;">s/d</div>
                                <div class="col-12 col-md-2">
                                    <input type="date" id="end_date" name="end_date" placeholder="Text" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Jenis Target</label></div>
                                <div class="col-12 col-md-5">
                                    <div class="form-check-inline form-check">
                                        <label for="inline-radio1" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Download" class="form-check-input">Download
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="type_target" name="type" value="Pendapatan" class="form-check-input">Proyeksi Pendapatan
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Target</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="email" id="goal" name="goal" placeholder="Goal" class="form-control">
                                    <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_pr["items"]); ?>' required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="textarea-input" class=" form-control-label">Remarks</label></div>
                                <div class="col-12 col-md-9"><textarea name="remarks" id="remarks" rows="5" placeholder="Remarks..." class="form-control"></textarea></div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Tanggal Target</label></div>
                                <div class="col-12 col-md-2">
                                    <input type="date" id="tgl_target" name="tgl_target" placeholder="Tanggal Target" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Capaian</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="text" id="capaian" name="capaian" placeholder="Capaian" class="form-control">
                                    <input type="hidden" class="form-control " id="id" name="id" value="0"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label"></label></div>
                                <div class="col-12 col-md-3">
                                    <button type="submit" class="btn btn-primary btn-sm" id="add-items"> Add Detail</button>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered" id="detail">
                                            <thead>
                                                <tr>
                                                    <th width="1%">ID</th>
                                                    <th>Tanggal Target</th>
                                                    <th width="20%">Capaian</th>
                                                    <th>Info</th>
                                                    <th width="5%">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info btn-sm" id="save"> Save </button>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('transaction/target') ?>">Cancel</a>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    $("#start_date").focus();

    pr = {
        data: {},
        processed: false,
        items: [],

        init: function(){
            this.grids = $('#detail').DataTable({
                "paging": false, 
                "bLengthChange": false, // disable show entries dan page
                "bFilter": false,
                "bInfo": false, // disable Showing 0 to 0 of 0 entries
                "bAutoWidth": false,
                "language": {
                    "emptyTable": "Tidak Ada Data"
                },
                "columnDefs": [
                    { // disable sorting on column process buttons
                    "targets": [1,2,3,4],
                    "orderable": false,
                    },
                    { 
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ],
                columns: [
                    { data: 'item_id'},
                    { data: 'tgl_target'}, 
                    { data: 'capaian', className: "text-right"}, 
                    { data: 'item_info',"visible": false}, 
                    { data: 'act', className: "text-center" }
                ],
            });

            this._set_items($('#sup_items').val());
            $('#add-items').click(pr.add_items);
            $('#save').click(pr.save);
        },

        _set_items: function(items){
            this.no_ajax = true;
            //
            if(items) items = JSON.parse(items);
            this.items = items;
            items.map(function(i,e){
                var data = {
                    item_id   : i.item_id,
                    tgl_target : i.tgl_target,
                    capaian  : i.capaian,
                    item_info : i.item_info
                };
                pr._addtogrid(data);
                pr._clearitem();
            });
            this.no_ajax = false;

        },

        _clearitem: function(){
            $('#tgl_target').val('');
            $('#capaian').val('');
        },

        add_items: function (e) {
            e.preventDefault();
            if($('#tgl_target').val()==''){
                toastr.error("<strong>Tanggal Target</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#tgl_target').focus();
                return false;
            }

            if(!$('#capaian').val()){
                toastr.error("<strong>Capaian</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $("#capaian").focus();
                return false;
            } 

            let tgl_target = $('#tgl_target').val();
            let capaian  = $('#capaian').val();
            let item_info = '';
            let id        = parseInt($('#id').val());
            var item_id    = id + 1;

            if(item_id){
                data = {
                    item_id : item_id,
                    tgl_target : tgl_target,
                    capaian : capaian,
                    item_info : item_info
                };

                pr._addtogrid(data);
                pr._clearitem();
                pr._focus();
            }

        },

        _addtogrid: function(data){
            let grids = this.grids;
            let exist = pr.grids.row('#'+data.item_id).index();
            //
            $('#id').val(data.item_id);

            data.act = '<button class="btn btn-outline-warning btn-sm" det-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);"><strong>X</strong></button>';
            data.DT_RowId = data.item_id;
            //
            if(exist===undefined){
                grids.row.add(data).draw(); 
            }else{ 
                toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
                return false;
            }

            if(this.no_ajax) return false;

            var myUrl ="../target/add_item";
            $.ajax({
                url    : myUrl,
                method : "POST",
                data: {
                    item_id   : data.item_id,
                    tgl_target : data.tgl_target,
                    capaian  : data.capaian,
                    item_info : data.item_info
                }
            });
        },

        _removefromgrid: function(el){
            let id = $(el).attr('det-id');
            pr.grids.row("#"+id).remove().draw();
            $.ajax({
                method: "GET",
                url: '../target/remove_item',
                data: {
                    index_id: id
                }
            });
            return false;
        },

        _focus: function(){
            $('#tgl_target').focus();
        },

        save: function(e){
            e.preventDefault();
            
            if($('#start_date').val()==''){
                toastr.error("<strong>Tanggal Awal Periode</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#start_date').focus();
                return false;
            }

            if($('#end_date').val()==''){
                toastr.error("<strong>Tanggal Akhir Periode</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#end_date').focus();
                return false;
            }

            if($('#goal').val()==''){
                toastr.error("<strong>Target</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
                $('#goal').focus();
                return false;
            }

            $.ajax({
                url: '../target/form_act',
                type : "POST",  
                dataType : 'json', // Jika ini digunakan, tidap perlu pakai $.parseJSON();
                data: {
                    start_date      : $('#start_date').val(),
                    end_date        : $('#end_date').val(),
                    goal            : $('#goal').val(),
                    remarks         : $('#remarks').val(),
                    target          : $('#type_target:checked').val()
                },
                success : function(resp){
                    // var myObj = $.parseJSON(resp); 
                    var myObj = resp;
                    if(myObj.status == 'ERROR INSERT' || myObj.status == false) {
                        toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
                        return false;
                    } else {
                        messages = 'Data Berhasil di Simpan ';
                        messages += "<hr>";

                        toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
                        
                        setTimeout(function () {
                            window.location.href = '../target/'; 
                        }, 2000);
                    }
                }
            });

            $('#save').prop("disabled",true);

        }



    };

    pr.init();

</script>
<?= $this->endSection() ?>