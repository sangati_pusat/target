<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Report RAB</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="<?= base_url('transaction/rab/report'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                
                                <div class="col col-md-2"><label for="email-input" class=" form-control-label">Project</label></div>
                                <div class="col-12 col-md-3">
                                    <select name="rab_id" id="rab_id" class="form-control-label" style="width: 300px !important;">
                                        <option value="">- Pilih Project</option>
                                        <?php 
                                        foreach ($data_project as $key => $value) {
                                            echo "<option value='".$value['rab_header_id']."'>".$value['rab_project']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-primary" type="submit" id="save" name="search" value="search"> Search</button>
                        </form>
                    </div>
                    <?php 
                    // test($search,1);
                    if($search!=''){    
                        // test($data_rab,1);
                    ?>
                    <div class="card-header">
                        <strong class="card-title">View Rencana Anggaran Biaya Project <?= $rab_header->rab_project; ?></strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2" width='3%'>No</th>
                                    <th>Items Name</th>
                                    <th colspan="2" align="center">RAB</th>
                                    <th colspan="2" align="center">Allocation</th>
                                    <th rowspan="2" width='10%'>Percent</th>
                                </tr>
                                <tr>
                                    <th>Items Name</th>
                                    <th>Qty</th>
                                    <th>Nominal</th>
                                    <th>Qty</th>
                                    <th>Nominal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0; 
                                foreach($rab_detail as $value){
                                    $no         = $no+1;
                                    // test($value,1);
                                ?>
                                <tr>
                                    <th><?= $no; ?>.</th>
                                    <th><?= $value->items_name; ?></th>
                                    <th><?= $value->qty_rab; ?></th>
                                    <th><?= $value->nom_rab; ?></th>
                                    <th><?= $value->use_qty; ?></th>
                                    <th><?= $value->us_nom; ?></th>
                                    <th><?= $value->us_nom; ?></th>
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<?= $this->endSection(); ?>

<?= $this->section('javascript') ?>
<script type="text/javascript">
    $("#rab_id").select2();
    $("#start_date").focus();
    $('#bootstrap-data-table').DataTable();
</script>
<?= $this->endSection() ?>