<?= $this->extend('body'); ?>
<?= $this->section('content'); ?>

<style>
.highcharts-figure, .highcharts-data-table table {
  min-width: 360px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<div class="content">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Report Dashboard</strong>
					</div>
					<div class="card-body card-block">
          	<form action="<?= base_url('home'); ?>" method="post" class="form-horizontal">
              <div class="row form-group">
                  <div class="col col-md-2"><label for="text-input" class=" form-control-label">Tanggal Awal</label></div>
                  <div class="col-12 col-md-3">
                      <input type="date" id="date_periode" name="date_awal" class="form-control">
                  </div>
                  <div class="col col-md-2"><label for="text-input" class=" form-control-label">Tanggal Akhir</label></div>
                  <div class="col-12 col-md-3">
                      <input type="date" id="date_periode" name="date_akhir" class="form-control">
                  </div>
              </div>
              <div class="row form-group">
                  <div class="col col-md-2"><label for="text-input" class=" form-control-label"></label></div>
                  <div class="col-12 col-md-3">
                  		<button class="btn btn-primary" type="submit" id="save" name="search" value="search"> Search</button>
                  </div>
              </div>
            </form>
          </div>
				</div>
			</div>
		</div>
		<?php 
		// test($search,1);
		if($search=='search'){
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="card" style="margin-bottom: 0px !important;">
					<div class="card-header">
						<strong class="card-title">Target Dari Tanggal <?= tgl_singkat($tanggal_awal).' s/d '.tgl_singkat($tanggal_akhir); ?></strong>
					</div>
				</div>
			</div>
			<div class="col-xl-6">
				<div class="card bg-flat-color-3 white-color">
					<div class="card-body text-center">
						<h1 >Download </h1>
					</div>
					<?php 
					if($header['download']!=''){
					?>
					<div class="card-body">
						<div class="card-body card-block ">							
							<div class="row form-group">
								<div class="col-1"></div>
                                <div class="col col-md-5"><label for="text-input" class=" form-control-label">Goal</label></div>
                                <div class="col-5 text-right">
									<?= money($header['download']['goal']); ?>
                                </div>
								<div class="col-1"></div>
 							</div>

							<div class="row form-group">
								<div class="col-1"></div>
								<div class="col col-md-5"><label for="text-input" class=" form-control-label">Tercapai</label></div>
                                <div class="col-5 text-right">
									<?= money($header['download']['capaian']); ?>									
                                </div>
								<div class="col-1"></div>
              </div>
							<div class="row form-group">
								<div class="col-1"></div>
								<div class="col col-md-5"><label for="text-input" class=" form-control-label">Persen</label></div>
                                <div class="col-5 text-right">
									<?= money($header['download']['persen']); ?> %
                                </div>
								<div class="col-1"></div>
              </div>
						</div> <!-- /.table-stats -->
					</div>
					<?php 
					}
					?>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->

			<div class="col-xl-6">
				<div class="card bg-flat-color-2 white-color">
					<div class="card-body text-center">
						<h1 >Proyeksi Pendapatan </h1>
					</div>
					<?php 
					if($header['download']!=''){
					?>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-1"></div>
                                <div class="col col-md-5"><label for="text-input" class=" form-control-label">Goal</label></div>
                                <div class="col-5 text-right">
									<?= money($header['pendapatan']['goal']); ?>
                                </div>
								<div class="col-1"></div>
                            </div>
							<div class="row form-group">
								<div class="col-1"></div>
								<div class="col col-md-5"><label for="text-input" class=" form-control-label">Tercapai</label></div>
                                <div class="col-5 text-right">
									<?= money($header['pendapatan']['capaian']); ?>									
                                </div>
								<div class="col-1"></div>
                            </div>
							<div class="row form-group">
								<div class="col-1"></div>
								<div class="col col-md-5"><label for="text-input" class=" form-control-label">Persen</label></div>
                                <div class="col-5 text-right">
									<?= money($header['pendapatan']['persen']); ?> %		
                                </div>
								<div class="col-1"></div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
					<?php 
					}
					?>
				</div> <!-- /.card -->
			</div>
		</div>
		<div class="row">
			<div class="col-xl-12">
				<div class="card bg-flat-color-1 white-color">
					<div class="card-body text-center">
						<h1 >Report CS </h1>
					</div>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-12">
									<figure class="highcharts-figure">
										<div id="container_cs"></div>	
									</figure>
								</div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->
		</div>
		<div class="row">
			<div class="col-xl-12">
				<div class="card bg-flat-color-6 white-color">
					<div class="card-body text-center">
						<h1 >Report FB Ads </h1>
					</div>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-12">
									<figure class="highcharts-figure">
										<div id="container_fb"></div>	
									</figure>
								</div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->
		</div>
		<div class="row">
			<div class="col-xl-6">
				<div class="card bg-flat-color-5 white-color">
					<div class="card-body text-center">
						<h1 >Umur </h1>
					</div>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-12">
									<figure class="highcharts-figure">
										<div id="container_age"></div>	
									</figure>
								</div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->
			<div class="col-xl-6">
				<div class="card bg-flat-color-4 white-color">
					<div class="card-body text-center">
						<h1 >Kelamin </h1>
					</div>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-12">
									<figure class="highcharts-figure">
										<div id="container_gender"></div>	
									</figure>
								</div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->
		</div>
		<div class="row">
			<div class="col-xl-12">
				<div class="card bg-flat-color-6 white-color">
					<div class="card-body text-center">
						<h1 >Report Search Engine Marketing </h1>
					</div>
					<div class="card-body">
						<div class="card-body card-block ">
							<div class="row form-group">
								<div class="col-12">
									<figure class="highcharts-figure">
										<div id="container_sem"></div>	
									</figure>
								</div>
                            </div>
						</div> <!-- /.table-stats -->
					</div>
				</div> <!-- /.card -->
			</div>  <!-- /.col-lg-8 -->
		</div>
	<?php 
	}
	?>
	</div><!-- .animated -->
</div><!-- .content -->

<div class="clearfix"></div>
<?php 
if($search=='search'){
$trn_tgl_cs 	= array();
foreach ($tgl_cs as $key => $value1) {
	array_push($trn_tgl_cs,$value1->date_periode);
}

$trn_value_cs_wa 	= array();
$trn_value_cs_per 	= array();
$trn_value_cs_pem 	= array();

foreach ($value_cs as $key => $value2) {
	array_push($trn_value_cs_wa,$value2->value_wa);
	array_push($trn_value_cs_per,$value2->value_permintaan);
	array_push($trn_value_cs_pem,$value2->value_pembayaran);
}

$trn_tgl_sem 	= array();
foreach ($tgl_cs as $key => $value3) {
	array_push($trn_tgl_sem,$value3->date_periode);
}

$trn_value_sem_ja 	= array();
$trn_value_sem_kl 	= array();
$trn_value_sem_in 	= array();

foreach ($value_sem as $key => $value4) {
	array_push($trn_value_sem_ja,$value4->value_jangkauan);
	array_push($trn_value_sem_kl,$value4->value_klik);
	array_push($trn_value_sem_in,$value4->value_install);
}

$trn_tgl_fb 	= array();
foreach ($tgl_fb as $key => $value6) {
	array_push($trn_tgl_fb,$value6->date_periode);
}

$trn_value_fb_ja 	= array();
$trn_value_fb_kl 	= array();
$trn_value_fb_in 	= array();

foreach ($value_fb as $key => $value5) {
	array_push($trn_value_fb_ja,$value5->value_jangkauan);
	array_push($trn_value_fb_kl,$value5->value_klik);
	array_push($trn_value_fb_in,$value5->value_install);
}

?>
<script type="text/javascript">
var vWa 	= <?= json_encode($trn_value_cs_wa); ?>.map(Number);
var vPer 	= <?= json_encode($trn_value_cs_per); ?>.map(Number);
var vPem 	= <?= json_encode($trn_value_cs_pem); ?>.map(Number);

Highcharts.chart('container_cs', {
	chart: {
    	type: 'line'
  	},
  	title: {
    	text: 'Report Customer Service'
  	},
  	xAxis: {
    	categories: <?= json_encode($trn_tgl_cs); ?>
  	},
  	yAxis: {
    	title: {
      		text: 'Nilai / Value'
   		}
  	},
  	plotOptions: {
    	line: {
      		dataLabels: {
        	enabled: true
      		},
      		enableMouseTracking: false
    	}
  	},
  	series: [
		{
			name: 'Whatsapp',
			data: vWa
		}, {
			name: 'Permintaan',
			data: vPer
		}, {
			name: 'Pembayaran',
			data: vPem
		}
	]
});

Highcharts.chart('container_age', {
  title: {
    text: 'REPORT UMUR',
    align: 'center',
    verticalAlign: 'top'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: -50,
        style: {
          fontWeight: 'bold',
          color: 'white'
        }
      },
      startAngle: 0,
      endAngle: 360,
      center: ['50%', '50%'],
      size: '100%'
    }
  },
  series: [{
    type: 'pie',
    name: 'Umur ',
    innerSize: '50%',
    data: [
      ['25 - 34', <?= $value_age->a25; ?>],
      ['35 - 44', <?= $value_age->a35; ?>],
      ['45 - 54', <?= $value_age->a45; ?>],
      ['55 - 64', <?= $value_age->a55; ?>],
      ['65 ++', <?= $value_age->a65; ?>]
    ]
  }]
});

Highcharts.chart('container_gender', {
  title: {
    text: 'REPORT KELAMIN',
    align: 'center',
    verticalAlign: 'top'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: -50,
        style: {
          fontWeight: 'bold',
          color: 'white'
        }
      },
      startAngle: 0,
      endAngle: 360,
      center: ['50%', '50%'],
      size: '100%'
    }
  },
  series: [{
    type: 'pie',
    name: 'Kelamin ',
    innerSize: '50%',
    data: [
      ['Perempuan', <?= $value_gender->wanita; ?>],
      ['Laki-Laki', <?= $value_gender->pria; ?>]
    ]
  }]
});

var vJa 	= <?= json_encode($trn_value_sem_ja); ?>.map(Number);
var vKl 	= <?= json_encode($trn_value_sem_kl); ?>.map(Number);
var vIn 	= <?= json_encode($trn_value_sem_in); ?>.map(Number);

Highcharts.chart('container_sem', {
	chart: {
    	type: 'line'
  	},
  	title: {
    	text: 'Report Search Engine Marketing'
  	},
  	xAxis: {
    	categories: <?= json_encode($trn_tgl_sem); ?>
  	},
  	yAxis: {
    	title: {
      		text: 'Nilai / Value'
   		}
  	},
  	plotOptions: {
    	line: {
      		dataLabels: {
        	enabled: true
      		},
      		enableMouseTracking: false
    	}
  	},
  	series: [
		{
			name: 'Jangkauan',
			data: vJa
		}, {
			name: 'Klik',
			data: vKl
		}, {
			name: 'Install',
			data: vIn
		}
	]
});

var vFbJa 	= <?= json_encode($trn_value_fb_ja); ?>.map(Number);
var vFbKl 	= <?= json_encode($trn_value_fb_kl); ?>.map(Number);
var vFbIn 	= <?= json_encode($trn_value_fb_in); ?>.map(Number);

Highcharts.chart('container_fb', {
	chart: {
    	type: 'line'
  	},
  	title: {
    	text: 'Report Fb Ads'
  	},
  	xAxis: {
    	categories: <?= json_encode($trn_tgl_fb); ?>
  	},
  	yAxis: {
    	title: {
      		text: 'Nilai / Value'
   		}
  	},
  	plotOptions: {
    	line: {
      		dataLabels: {
        	enabled: true
      		},
      		enableMouseTracking: false
    	}
  	},
  	series: [
		{
			name: 'Jangkauan',
			data: vFbJa
		}, {
			name: 'Klik',
			data: vFbKl
		}, {
			name: 'Install',
			data: vFbIn
		}
	]
});
</script>
<?php
}
?>

<?= $this->endSection(); ?>