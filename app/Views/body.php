<?php 
$session = \Config\Services::session();
if($session->get('set_session')['name']){
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard - PT. Beberes Rumah Sejahtera</title>
    <meta name="description" content="Dashboard - PT. Beberes Rumah Sejahtera">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://nyaman.online/assets/image/logo-nyaman-icon.png">
    <link rel="shortcut icon" href="https://nyaman.online/assets/image/logo-nyaman-icon.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/lib/chosen/chosen.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/toastr.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/select2.min.css" rel="stylesheet">

    

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>
<?php 
    $session = session();
    $amenu    = array();
    $asubmenu = array();

    foreach ($session->get('set_session')['submenu'] as $key => $value) {
        $asubmenu[$value->menu_group][]=$value;
    }
    // test($asubmenu[$value->menu_group],1);
?>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li >
                        <a href="<?= base_url('home'); ?>"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <?php 
                    foreach ($session->get('set_session')['menu'] as $key => $menu) { 
                    ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="<?= $menu->icon; ?>"></i><?= $menu->menu_group; ?></a>
                        <ul class="sub-menu children dropdown-menu">   
                            <?php 
                            foreach ($asubmenu[$menu->menu_group] as $key => $value) {
                            ?>
                                <li><a href="<?= base_url($value->url); ?>"><?=  $value->menu_name; ?></a></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                    }
                    ?>
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-puzzle-piece"></i>Target Operation</a>
                        <ul class="sub-menu children dropdown-menu">   
                            <li ><a href="<?= base_url('transaction/target'); ?>">Target </a></li>
                            <li ><a href="<?= base_url('transaction/cs_target'); ?>">Customer Service </a></li>
                            <li ><a href="<?= base_url(''); ?>">FB ADS </a></li>
                            <li ><a href="<?= base_url('transaction/sem_report'); ?>">SEM </a></li>
                            <li ><a href="<?= base_url('transaction/gender_report'); ?>">Gender </a></li>
                            <li ><a href="<?= base_url('transaction/age_report'); ?>">Age </a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Monitoring Project</a>
                        <ul class="sub-menu children dropdown-menu">                            
                            <li><a href="<?= base_url('transaction/rab'); ?>"></i>Budget Plan </a></li>
                            <li><a href="<?= base_url('transaction/Item_allocation_header'); ?>"></i>items allocation </a></li>
                            <li><a href="<?= base_url('transaction/report_rab'); ?>"></i>Report </a></li>
                        </ul>
                    </li> -->
                    <!-- <li >
                        <a href="<?= base_url('transaction/project'); ?>"><i class="menu-icon fa fa-fire"></i>Project </a>
                    </li> -->                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->

    <script>
        var baseUrl = '<?= base_url(); ?>';
    </script>

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= base_url('home'); ?>"><img src="<?= base_url('/assets/images/logo-nyaman-hr.png') ?>" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="<?= base_url('/assets/images/logo-nyaman-hr.png') ?>" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $session->get('set_session')['name']; ?>
                        </a>

                        <div class="user-menu dropdown-menu">
                            <!-- <a class="nav-link" href="#"><i class="fa fa-user"></i>My Profile</a>
                            <a class="nav-link" href="#"><i class="fa fa-bell-o"></i>Notifications <span class="count">13</span></a>
                            <a class="nav-link" href="#"><i class="fa fa-cog"></i>Settings</a> -->
                            <a class="nav-link" href="<?= base_url('logout'); ?>"><i class="fa fa-power-off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- /header -->
        <!-- Header-->

        <?= $this->renderSection('content'); ?>    


    </div>


</div><!-- .animated -->
</div><!-- .content -->
    <!-- <div class="clearfix"></div> -->

    <!-- <footer class="site-footer">
        <div class="footer-inner bg-white">
            <div class="row">
                <div class="col-sm-6">
                    Copyright &copy; 2018 Ela Admin
                </div>
                <div class="col-sm-6 text-right">
                    Designed by <a href="https://colorlib.com">Colorlib</a>
                </div>
            </div>
        </div>
    </footer> -->


</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/main.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/chosen/chosen.jquery.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/datatables.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/jszip.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/toastr.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/select2.full.min.js"></script>

<?= $this->renderSection('javascript'); ?>

</body>
</html>
<?php 
}else{

}
?>