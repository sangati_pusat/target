<?php namespace App\Models\Transaction; 
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Jun 28, 2021                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
use CodeIgniter\Model;

class Target_model_01 extends Model
{
     /* START PRIVATE VARIABLES */
     protected $DBGroup = '';
     protected $table = 'trn_header';
     protected $primaryKey = 'id_header';
     protected $returnType = 'array';
     protected $db = null;

     protected $idHeader;
     protected $periodeStart;
     protected $periodeEnd;
     protected $goal;
     protected $capaian;
     protected $remarks;
     protected $persen;
     protected $type;
     protected $inputPic;
     protected $inputDate;
     protected $editPic;
     protected $editDate;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */ 
     public function __construct()
     {
     	parent::__construct();
     	$this->db = \Config\Database::connect($this->DBGroup, false);
        $this->idHeader = 0;
        $this->periodeStart = '0000-00-00';
        $this->periodeEnd = '0000-00-00';
        $this->goal = 0;
        $this->capaian = 0;
        $this->remarks = '';
        $this->persen = 0;
        $this->type = '';
        $this->inputPic = '';
        $this->inputDate = '1700-01-01 00:00:00';
        $this->editPic = '';
        $this->editDate = '1700-01-01 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setIdHeader($aIdHeader)
     {
     	$this->idHeader = $aIdHeader;
     }
     public function getIdHeader()
     {
     	return $this->idHeader;
     }
     public function setPeriodeStart($aPeriodeStart)
     {
     	$this->periodeStart = $aPeriodeStart;
     }
     public function getPeriodeStart()
     {
     	return $this->periodeStart;
     }
     public function setPeriodeEnd($aPeriodeEnd)
     {
     	$this->periodeEnd = $aPeriodeEnd;
     }
     public function getPeriodeEnd()
     {
     	return $this->periodeEnd;
     }
     public function setGoal($aGoal)
     {
     	$this->goal = $aGoal;
     }
     public function getGoal()
     {
     	return $this->goal;
     }
     public function setCapaian($aCapaian)
     {
     	$this->capaian = $aCapaian;
     }
     public function getCapaian()
     {
     	return $this->capaian;
     }
     public function setRemarks($aRemarks)
     {
     	$this->remarks = $aRemarks;
     }
     public function getRemarks()
     {
     	return $this->remarks;
     }
     public function setPersen($aPersen)
     {
     	$this->persen = $aPersen;
     }
     public function getPersen()
     {
     	return $this->persen;
     }
     public function setType($aType)
     {
     	$this->type = $aType;
     }
     public function getType()
     {
     	return $this->type;
     }
     public function setInputPic($aInputPic)
     {
     	$this->inputPic = $aInputPic;
     }
     public function getInputPic()
     {
     	return $this->inputPic;
     }
     public function setInputDate($aInputDate)
     {
     	$this->inputDate = $aInputDate;
     }
     public function getInputDate()
     {
     	return $this->inputDate;
     }
     public function setEditPic($aEditPic)
     {
     	$this->editPic = $aEditPic;
     }
     public function getEditPic()
     {
     	return $this->editPic;
     }
     public function setEditDate($aEditDate)
     {
     	$this->editDate = $aEditDate;
     }
     public function getEditDate()
     {
     	return $this->editDate;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function ins()
     {
     	if($this->idHeader =="" || $this->idHeader == NULL) 
     	{
          	$this->idHeader = 0; 
     	}
     	if($this->periodeStart =="" || $this->periodeStart == NULL) 
     	{
          	$this->periodeStart = '1700-01-01'; 
     	}
     	if($this->periodeEnd =="" || $this->periodeEnd == NULL) 
     	{
          	$this->periodeEnd = '1700-01-01'; 
     	}
     	if($this->goal =="" || $this->goal == NULL) 
     	{
          	$this->goal = 0; 
     	}
     	if($this->capaian =="" || $this->capaian == NULL) 
     	{
          	$this->capaian = 0; 
     	}
     	if($this->remarks =="" || $this->remarks == NULL) 
     	{
          	$this->remarks = ''; 
     	}
     	if($this->persen =="" || $this->persen == NULL) 
     	{
          	$this->persen = 0; 
     	}
     	if($this->type =="" || $this->type == NULL) 
     	{
          	$this->type = ''; 
     	}
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{
          	$this->inputPic = ''; 
     	}
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	}
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{
          	$this->editPic = ''; 
     	}
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{
          	$this->editDate = '1700-01-01 00:00:00'; 
     	}

     	$stQuery  = "INSERT INTO ".$this->table." ";
     	$stQuery .= "( ";
     	$stQuery .=   "id_header,"; 
     	$stQuery .=   "periode_start,"; 
     	$stQuery .=   "periode_end,"; 
     	$stQuery .=   "goal,"; 
     	$stQuery .=   "capaian,"; 
     	$stQuery .=   "remarks,"; 
     	$stQuery .=   "persen,"; 
     	$stQuery .=   "type,"; 
     	$stQuery .=   "input_pic,"; 
     	$stQuery .=   "input_date"; 
     	// $stQuery .=   "edit_pic,"; 
     	// $stQuery .=   "edit_date"; 
     	$stQuery .= ") "; 
     	$stQuery .= "VALUES "; 
     	$stQuery .= "( "; 
     	$stQuery .=   " ".$this->idHeader.",";
     	$stQuery .=   "'".$this->db->escapeString($this->periodeStart)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->periodeEnd)."',";
     	$stQuery .=   " ".$this->goal.",";
     	$stQuery .=   " ".$this->capaian.",";
     	$stQuery .=   "'".$this->db->escapeString($this->remarks)."',";
     	$stQuery .=   " ".$this->persen.",";
     	$stQuery .=   "'".$this->db->escapeString($this->type)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputDate)."'";
     	$stQuery .= "); "; 
        // test($stQuery,1);
		return $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START QUERY INSERT */
     public function getInsQuery()
     {
     	if($this->idHeader =="" || $this->idHeader == NULL) 
     	{
          	$this->idHeader = 0; 
     	}
     	if($this->periodeStart =="" || $this->periodeStart == NULL) 
     	{
          	$this->periodeStart = '1700-01-01'; 
     	}
     	if($this->periodeEnd =="" || $this->periodeEnd == NULL) 
     	{
          	$this->periodeEnd = '1700-01-01'; 
     	}
     	if($this->goal =="" || $this->goal == NULL) 
     	{
          	$this->goal = 0; 
     	}
     	if($this->capaian =="" || $this->capaian == NULL) 
     	{
          	$this->capaian = 0; 
     	}
     	if($this->remarks =="" || $this->remarks == NULL) 
     	{
          	$this->remarks = ''; 
     	}
     	if($this->persen =="" || $this->persen == NULL) 
     	{
          	$this->persen = 0; 
     	}
     	if($this->type =="" || $this->type == NULL) 
     	{
          	$this->type = ''; 
     	}
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{
          	$this->inputPic = ''; 
     	}
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	}
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{
          	$this->editPic = ''; 
     	}
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{
          	$this->editDate = '1700-01-01 00:00:00'; 
     	}

     	$stQuery  = "INSERT INTO ".$this->table." ";
     	$stQuery .= "( ";
     	$stQuery .=   "id_header,"; 
     	$stQuery .=   "periode_start,"; 
     	$stQuery .=   "periode_end,"; 
     	$stQuery .=   "goal,"; 
     	$stQuery .=   "capaian,"; 
     	$stQuery .=   "remarks,"; 
     	$stQuery .=   "persen,"; 
     	$stQuery .=   "type,"; 
     	$stQuery .=   "input_pic,"; 
     	$stQuery .=   "input_date,"; 
     	$stQuery .=   "edit_pic,"; 
     	$stQuery .=   "edit_date"; 
     	$stQuery .= ") "; 
     	$stQuery .= "VALUES "; 
     	$stQuery .= "( "; 
     	$stQuery .=   " ".$this->idHeader.",";
     	$stQuery .=   "'".$this->db->escapeString($this->periodeStart)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->periodeEnd)."',";
     	$stQuery .=   " ".$this->goal.",";
     	$stQuery .=   " ".$this->capaian.",";
     	$stQuery .=   "'".$this->db->escapeString($this->remarks)."',";
     	$stQuery .=   " ".$this->persen.",";
     	$stQuery .=   "'".$this->db->escapeString($this->type)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->inputDate)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editPic)."',";
     	$stQuery .=   "'".$this->db->escapeString($this->editDate)."'";
     	$stQuery .= "); "; 
     	return $stQuery; 
     }
     /* END QUERY INSERT */
     /* START UPDATE */
     public function upd($id)
     {
     	if($this->idHeader =="" || $this->idHeader == NULL) 
     	{ 
          	$this->idHeader = 0; 
     	} 
     	if($this->periodeStart =="" || $this->periodeStart == NULL) 
     	{ 
          	$this->periodeStart = '1700-01-01'; 
     	} 
     	if($this->periodeEnd =="" || $this->periodeEnd == NULL) 
     	{ 
          	$this->periodeEnd = '1700-01-01'; 
     	} 
     	if($this->goal =="" || $this->goal == NULL) 
     	{ 
          	$this->goal = 0; 
     	} 
     	if($this->capaian =="" || $this->capaian == NULL) 
     	{ 
          	$this->capaian = 0; 
     	} 
     	if($this->remarks =="" || $this->remarks == NULL) 
     	{ 
          	$this->remarks = ''; 
     	} 
     	if($this->persen =="" || $this->persen == NULL) 
     	{ 
          	$this->persen = 0; 
     	} 
     	if($this->type =="" || $this->type == NULL) 
     	{ 
          	$this->type = ''; 
     	} 
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{ 
          	$this->inputPic = ''; 
     	} 
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{ 
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	} 
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{ 
          	$this->editPic = ''; 
     	} 
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{ 
          	$this->editDate = '1700-01-01 00:00:00'; 
     	} 

     	$stQuery  = "UPDATE ".$this->table." "; 
     	$stQuery .= "SET "; 
     	// $stQuery .=   'id_header ='.$this->idHeader.','; 
     	$stQuery .=   "periode_start ='".$this->db->escapeString($this->periodeStart)."', "; 
     	$stQuery .=   "periode_end ='".$this->db->escapeString($this->periodeEnd)."', "; 
     	$stQuery .=   'goal ='.$this->goal.','; 
     	$stQuery .=   'capaian ='.$this->capaian.','; 
     	$stQuery .=   "remarks ='".$this->db->escapeString($this->remarks)."', "; 
     	$stQuery .=   'persen ='.$this->persen.','; 
     	$stQuery .=   "type ='".$this->db->escapeString($this->type)."', "; 
     	// $stQuery .=   "input_pic ='".$this->db->escapeString($this->inputPic)."', "; 
     	// $stQuery .=   "input_date ='".$this->db->escapeString($this->inputDate)."', "; 
     	$stQuery .=   "edit_pic ='".$this->db->escapeString($this->editPic)."', "; 
     	$stQuery .=   "edit_date ='".$this->db->escapeString($this->editDate)."' "; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'id_header = '.$this->db->escapeString($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START QUERY UPDATE */
     public function getUpdQuery($id)
     {
     	if($this->idHeader =="" || $this->idHeader == NULL) 
     	{ 
          	$this->idHeader = 0; 
     	} 
     	if($this->periodeStart =="" || $this->periodeStart == NULL) 
     	{ 
          	$this->periodeStart = '1700-01-01'; 
     	} 
     	if($this->periodeEnd =="" || $this->periodeEnd == NULL) 
     	{ 
          	$this->periodeEnd = '1700-01-01'; 
     	} 
     	if($this->goal =="" || $this->goal == NULL) 
     	{ 
          	$this->goal = 0; 
     	} 
     	if($this->capaian =="" || $this->capaian == NULL) 
     	{ 
          	$this->capaian = 0; 
     	} 
     	if($this->remarks =="" || $this->remarks == NULL) 
     	{ 
          	$this->remarks = ''; 
     	} 
     	if($this->persen =="" || $this->persen == NULL) 
     	{ 
          	$this->persen = 0; 
     	} 
     	if($this->type =="" || $this->type == NULL) 
     	{ 
          	$this->type = ''; 
     	} 
     	if($this->inputPic =="" || $this->inputPic == NULL) 
     	{ 
          	$this->inputPic = ''; 
     	} 
     	if($this->inputDate =="" || $this->inputDate == NULL) 
     	{ 
          	$this->inputDate = '1700-01-01 00:00:00'; 
     	} 
     	if($this->editPic =="" || $this->editPic == NULL) 
     	{ 
          	$this->editPic = ''; 
     	} 
     	if($this->editDate =="" || $this->editDate == NULL) 
     	{ 
          	$this->editDate = '1700-01-01 00:00:00'; 
     	} 

     	$stQuery  = "UPDATE ".$this->table." "; 
     	$stQuery .= "SET "; 
     	$stQuery .=   'id_header ='.$this->idHeader.','; 
     	$stQuery .=   "periode_start ='".$this->db->escapeString($this->periodeStart)."', "; 
     	$stQuery .=   "periode_end ='".$this->db->escapeString($this->periodeEnd)."', "; 
     	$stQuery .=   'goal ='.$this->goal.','; 
     	$stQuery .=   'capaian ='.$this->capaian.','; 
     	$stQuery .=   "remarks ='".$this->db->escapeString($this->remarks)."', "; 
     	$stQuery .=   'persen ='.$this->persen.','; 
     	$stQuery .=   "type ='".$this->db->escapeString($this->type)."', "; 
     	$stQuery .=   "input_pic ='".$this->db->escapeString($this->inputPic)."', "; 
     	$stQuery .=   "input_date ='".$this->db->escapeString($this->inputDate)."', "; 
     	$stQuery .=   "edit_pic ='".$this->db->escapeString($this->editPic)."', "; 
     	$stQuery .=   "edit_date ='".$this->db->escapeString($this->editDate)."' "; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'id_header = '.$this->db->escapeString($id).''; 
     	return $stQuery; 
     }
     /* END QUERY UPDATE */
     /* START DELETE */
     public function del($id)
     {
     	$stQuery  = "DELETE FROM ".$this->table." ";      
     	$stQuery .= "WHERE id_header = ".$this->db->escapeString($id)."";
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$stQuery  = "SELECT * FROM ".$this->table." WHERE is_active='1' ORDER BY id_header DESC"; 
     	$query  = $this->db->query($stQuery); 
     	$arrayList = $query->getResultArray(); 
     	return $arrayList; 
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$stQuery  = "SELECT * FROM ".$this->table." ";     
     	$stQuery .= "WHERE id_header = ".$this->db->escapeString($id)."";
     	$query  = $this->db->query($stQuery); 
     	$arrayRow = $query->getRowArray(); 
     	return $arrayRow; 
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function setObjectById($id)
     {
     	$stQuery  = "SELECT * FROM ".$this->table." "; 
     	$stQuery .= "WHERE id_header = ".$this->db->escapeString($id)."";
     	$query  = $this->db->query($stQuery); 
     	$arrayRow = $query->getRowArray(); 
     	$this->idHeader = $arrayRow['id_header']; 
     	$this->periodeStart = $arrayRow['periode_start']; 
     	$this->periodeEnd = $arrayRow['periode_end']; 
     	$this->goal = $arrayRow['goal']; 
     	$this->capaian = $arrayRow['capaian']; 
     	$this->remarks = $arrayRow['remarks']; 
     	$this->persen = $arrayRow['persen']; 
     	$this->type = $arrayRow['type']; 
     	$this->inputPic = $arrayRow['input_pic']; 
     	$this->inputDate = $arrayRow['input_date']; 
     	$this->editPic = $arrayRow['edit_pic']; 
     	$this->editDate = $arrayRow['edit_date']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->idHeader = 0; 
     	$this->periodeStart = '1700-01-01'; 
     	$this->periodeEnd = '1700-01-01'; 
     	$this->goal = 0; 
     	$this->capaian = 0; 
     	$this->remarks = ''; 
     	$this->persen = 0; 
     	$this->type = ''; 
     	$this->inputPic = ''; 
     	$this->inputDate = '1700-01-01 00:00:00'; 
     	$this->editPic = ''; 
     	$this->editDate = '1700-01-01 00:00:00'; 
     }
     /* END OF RESET VALUES */
     /* START OF ID GENERATOR */
     public function generateId($columnId)
     {
		$strSql = "SELECT ";
		$strSql .= " CASE WHEN id_year = curr_ym THEN ";
		$strSql .= "   CONCAT(id_year,LPAD(CAST( (CAST(RIGHT(fi,5) AS INTEGER)+1) AS CHAR),5,'0')) ";
		$strSql .= " ELSE ";
		$strSql .= "   CONCAT(curr_ym, '00001') ";
		$strSql .= " END AS doc_id ";
		$strSql .= "FROM ";
		$strSql .= " (SELECT ";
		$strSql .= "   MAX(".$columnId.") fi,";
		$strSql .= "   LEFT(".$columnId.",4) id_year,";
		$strSql .= "   current_date dt,";
		$strSql .= "   RIGHT(".$columnId.", 5) curr_id,";
		$strSql .= "   CONCAT(";
		$strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2), ";
		$strSql .= "   LPAD(";
		$strSql .= "      CAST(DATE_FORMAT(NOW(), '%m') AS CHAR),2,'0'";
		$strSql .= "    )";
		$strSql .= "   ) curr_ym,";
		$strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2) tYear,";
		$strSql .= "   CAST(DATE_FORMAT(NOW(), '%m') AS CHAR) tMonth ";
		$strSql .= " FROM ";
		$strSql .= "   ".$this->table." ";
		$strSql .= " GROUP BY ".$columnId.") a ";
		$strSql .= " ORDER BY doc_id DESC LIMIT 1 ";
		$query  = $this->db->query($strSql); 
		$arrayRow = $query->getRowArray(); 
		if(!isset($arrayRow))
		{
			$curYM = date('ym');
			$arrayRow['doc_id'] = $curYM.'00001';
		}
		return $arrayRow;
     }
     /* END OF ID GENERATOR */

    public function cek_id(){
        $db = \Config\Database::connect($this->DBGroup);
		$query 		= "SELECT IFNULL(MAX(id_header),0)+1 id_header FROM db_target.trn_header ";
		
		return $this->db->query($query)->getRow();
    }

	public function update_status($id){
        $db = \Config\Database::connect($this->DBGroup);
		$query 		= "UPDATE db_target.trn_header SET is_active = '0' WHERE id_header = '".$id."' ";		
		return $this->db->query($query);
    }

	public function dashboard_header($type,$tgl_awal,$tgl_akhir){
		$db = \Config\Database::connect($this->DBGroup);
		$query 		= "SELECT goal,capaian,persen FROM trn_header WHERE `type` = '".$type."' AND is_active='1' AND periode_start>='".$tgl_awal."' AND 
                     periode_end<='".$tgl_akhir."'";
      
		return $this->db->query($query)->getRowArray();
	}
    
}
